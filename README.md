# api-rest-cra-ctes

> Api-Rest-CRA-Ctes is a custom Node Express project.

To carry out this project I developed a small custom framework in node.
This framework automatically loads the configurations of the different environments, as well as facilitating the registration of the different http requests.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm start

# run tests
npm test
```