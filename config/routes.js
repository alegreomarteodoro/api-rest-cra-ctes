//For private sections use the checkToken middleware
var checkToken = require('../app/middlewares/check_token');

module.exports = function (app, express, routes) {

    /***********************
     * Default
     ***********************/
    app.get('/', routes.api.v1.home.index);


    /************************
     * API V1
     ***********************/
    var api_v1 = express.Router();

    // default
    api_v1.get('/', routes.api.v1.home.index);


    // payments
    api_v1.route('/payments/create-monthly-fee')
        .post(checkToken, routes.api.v1.payments.createMonthlyFee);

    api_v1.route('/payments/upgrade-base-amount')
        .post(checkToken, routes.api.v1.payments.upgradeBaseAmount);

    api_v1.route('/payments/process-payment')
        .get(routes.api.v1.payments.processPayment)
        .post(routes.api.v1.payments.processPayment);

    api_v1.route('/payments/user-payments')
        .get(checkToken, routes.api.v1.payments.getUserPayments);

    api_v1.route('/payments')
        .get(checkToken, routes.api.v1.payments.getAllPayments)
        .put(checkToken, routes.api.v1.payments.edit)
        .post(checkToken, routes.api.v1.payments.create)
        .delete(checkToken, routes.api.v1.payments.delete);

    api_v1.route('/payments/pay-fee/:_id')
        .get(checkToken, routes.api.v1.payments.payFee);

    api_v1.route('/payments/:_id')
        .get(checkToken, routes.api.v1.payments.get)
        .delete(checkToken, routes.api.v1.payments.delete);


    // paymentRulesForFee
    api_v1.route('/payment-rules-for-fee')
        .get(checkToken, routes.api.v1.payment_rules_for_fee.list)
        .post(checkToken, routes.api.v1.payment_rules_for_fee.create)
        .put(checkToken, routes.api.v1.payment_rules_for_fee.edit)
        .delete(checkToken, routes.api.v1.payment_rules_for_fee.delete);

    api_v1.route('/payment-rules-for-fee/:_id')
        .get(checkToken, routes.api.v1.payment_rules_for_fee.get)
        .delete(checkToken, routes.api.v1.payment_rules_for_fee.delete);


    // paymentRulesForTuition
    api_v1.route('/payment-rules-for-tuition')
        .get(checkToken, routes.api.v1.payment_rules_for_tuition.list)
        .post(checkToken, routes.api.v1.payment_rules_for_tuition.create)
        .put(checkToken, routes.api.v1.payment_rules_for_tuition.edit)
        .delete(checkToken, routes.api.v1.payment_rules_for_tuition.delete);

    api_v1.route('/payment-rules-for-tuition/:_id')
        .get(checkToken, routes.api.v1.payment_rules_for_tuition.get)
        .delete(checkToken, routes.api.v1.payment_rules_for_tuition.delete);


    // studentFile
    api_v1.route('/student-files/get-by-user')
        .post(routes.api.v1.student_files.getByUser);

    api_v1.route('/student-files/get-list-students-of-current-academic-year')
        .get(checkToken, routes.api.v1.student_files.listStudentsOfCurrentAcademicYear);

    api_v1.route('/student-files')
        .get(routes.api.v1.student_files.list)
        .post(routes.api.v1.student_files.create)
        .put(routes.api.v1.student_files.edit);


    // users
    api_v1.route('/users')
        .get(checkToken, routes.api.v1.users.list)
        .post(routes.api.v1.users.register)
        .put(checkToken, routes.api.v1.users.edit)
        .delete(checkToken, routes.api.v1.users.delete);

    api_v1.route('/users/:_id')
        .get(checkToken, routes.api.v1.users.get)
        .delete(checkToken, routes.api.v1.users.delete);

    api_v1.route('/users/veriry-account/:activationCode')
        .get(routes.api.v1.users.veriryAccount);


    // roles
    api_v1.route('/roles')
        .get(checkToken, routes.api.v1.roles.list)
        .post(checkToken, routes.api.v1.roles.create)
        .put(checkToken, routes.api.v1.roles.edit)
        .delete(checkToken, routes.api.v1.roles.delete);

    api_v1.route('/roles/:_id')
        .get(checkToken, routes.api.v1.roles.get)
        .delete(checkToken, routes.api.v1.roles.delete);


    // permissions
    api_v1.route('/permissions')
        .get(checkToken, routes.api.v1.permissions.list)
        .post(checkToken, routes.api.v1.permissions.create)
        .put(checkToken, routes.api.v1.permissions.edit)
        .delete(checkToken, routes.api.v1.permissions.delete);

    api_v1.route('/permissions/:_id')
        .get(checkToken, routes.api.v1.permissions.get)
        .delete(checkToken, routes.api.v1.permissions.delete);


    // careers
    api_v1.route('/careers')
        .get(checkToken, routes.api.v1.careers.list)
        .post(checkToken, routes.api.v1.careers.create)
        .put(checkToken, routes.api.v1.careers.edit)
        .delete(checkToken, routes.api.v1.careers.delete);

    api_v1.route('/careers/:_id')
        .get(checkToken, routes.api.v1.careers.get)
        .delete(checkToken, routes.api.v1.careers.delete);


    // examination tables
    api_v1.route('/examination-tables')
        .get(checkToken, routes.api.v1.examination_tables.list)
        .post(checkToken, routes.api.v1.examination_tables.create)
        .put(checkToken, routes.api.v1.examination_tables.edit)
        .delete(checkToken, routes.api.v1.examination_tables.delete);

    api_v1.route('/examination-tables/:_id')
        .get(checkToken, routes.api.v1.examination_tables.get)
        .delete(checkToken, routes.api.v1.examination_tables.delete);


    // registration to examination tables
    api_v1.route('/registration-to-examination-tables')
        .get(checkToken, routes.api.v1.registration_to_examination_table.get)
        .put(checkToken, routes.api.v1.registration_to_examination_table.edit)
        .post(checkToken, routes.api.v1.registration_to_examination_table.create);

    api_v1.route('/list-of-registration-to-examination-tables')
        .get(checkToken, routes.api.v1.registration_to_examination_table.list);

    // registration to examination tables
    api_v1.route('/registration-to-examination-tables/download')
        .post(checkToken, routes.api.v1.registration_to_examination_table.download);


    // authentication
    api_v1.route('/authenticate')
        .post(routes.api.v1.auth.authenticate);

    //signup
    api_v1.route('/signup')
        .post(routes.api.v1.users.register);

    // config
    api_v1.route('/config')
        .get(checkToken, routes.api.v1.config.list)
        .put(checkToken, routes.api.v1.config.edit);

    api_v1.route('/config/:key')
        .get(checkToken, routes.api.v1.config.get);


    // registration to career

    // registration to examination tables
    api_v1.route('/registration-to-career/download')
        .post(checkToken, routes.api.v1.registration_to_career.download);

    api_v1.route('/registration-to-career')
        .get(checkToken, routes.api.v1.registration_to_career.list)
        .post(checkToken, routes.api.v1.registration_to_career.get)
        .put(checkToken, routes.api.v1.registration_to_career.edit);

    api_v1.route('/registration-to-career/:_id')
        .get(checkToken, routes.api.v1.registration_to_career.get)
        .delete(checkToken, routes.api.v1.registration_to_career.delete);



    // stock of ambos
    api_v1.route('/stock-of-ambos')
        .get(checkToken, routes.api.v1.stock_of_ambos.list)
        .post(checkToken, routes.api.v1.stock_of_ambos.create)
        .put(checkToken, routes.api.v1.stock_of_ambos.edit)
        .delete(checkToken, routes.api.v1.stock_of_ambos.delete);

    api_v1.route('/stock-of-ambos/get-number-of-students-enrolled')
        .get(routes.api.v1.stock_of_ambos.getNumberOfStudentsEnrolled);

    api_v1.route('/stock-of-ambos/:_id')
        .get(checkToken, routes.api.v1.stock_of_ambos.get)
        .delete(checkToken, routes.api.v1.stock_of_ambos.delete);


    // delivery of ambos
    api_v1.route('/delivery-of-ambos')
        .get(checkToken, routes.api.v1.delivery_of_ambos.list)
        .post(checkToken, routes.api.v1.delivery_of_ambos.create)
        .put(checkToken, routes.api.v1.delivery_of_ambos.edit)
        .delete(checkToken, routes.api.v1.delivery_of_ambos.delete);


    api_v1.route('/delivery-of-ambos/:_id')
        .get(checkToken, routes.api.v1.delivery_of_ambos.get)
        .delete(checkToken, routes.api.v1.delivery_of_ambos.delete);


    //Esto se uso para actualizar los estados de las inscripciones a las carreras
    /*api_v1.route('/sincronize-new-status')
        .get(routes.api.v1.registration_to_career.sincronizeRegistrationToCareers);*/


    app.use('/api/v1', api_v1);
};