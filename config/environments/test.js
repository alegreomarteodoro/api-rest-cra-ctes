
var errorHandler = require('errorhandler');

module.exports = function(app) {

    if ('test' == app.get('env')) {
        app.set('ip', '127.0.0.1');
        app.set('port', 3000);

        app.set('dbname', 'cruzroja_test');

        app.use(errorHandler());

        app.set('google.clientID', '450379005578.apps.googleusercontent.com');
        app.set('google.clientSecret', 'o7ZYhPXb2IgRNO9qN29eJI9I');
        app.set('google.callbackURL', 'http://localhost:8080/api/v1/auth/google/callback');

        app.set('twitter.clientID', 'o5R6BAmw21BfU0SWfP5HES9yT');
        app.set('twitter.clientSecret', '0FUEZdBhqHB8mbM04EUJjx1VWmYIpfWlKQejZifXC6C5H8JxYy');
        app.set('twitter.callbackURL', 'http://localhost:8080/api/v1/auth/twitter/callback');

        app.set('facebook.clientID', '387589304717533');
        app.set('facebook.clientSecret', 'f8a31b33f0132782535946151f45b667');
        app.set('facebook.callbackURL', 'http://localhost:8080/api/v1/auth/facebook/callback');

        app.use(function (req, res, next) {
            res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,x-access-token');
            res.setHeader('Access-Control-Allow-Credentials', true);
            next();
        });
    };

};