var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var mongooseAggregatePaginate = require('mongoose-aggregate-paginate');

var schemas = {
    userSchema : require('../../app/schemas/user'),
    roleSchema: require('../../app/schemas/role'),
    permissionSchema: require('../../app/schemas/permission'),
    careerSchema: require('../../app/schemas/career'),
    studentFileSchema: require('../../app/schemas/student_file'),
    configSchema: require('../../app/schemas/config'),
    logSchema: require('../../app/schemas/log'),
    registrationToCareerSchema: require('../../app/schemas/registration_to_career'),
    payments: require('../../app/schemas/payments'),
    paymentRulesForFee: require('../../app/schemas/payment_rules_for_fee'),
    paymentRulesForTuition: require('../../app/schemas/payment_rules_for_tuition'),
    examinationTables: require('../../app/schemas/examination_tables'),
    registrationToExaminationTable: require('../../app/schemas/registration_to_examination_table'),
    stockOfAmbos: require('../../app/schemas/stock_of_ambos'),
    deliveryOfAmbos: require('../../app/schemas/delivery_of_ambos')
};

schemas.registrationToCareerSchema.plugin(mongoosePaginate);
schemas.userSchema.plugin(mongooseAggregatePaginate);
schemas.payments.plugin(mongooseAggregatePaginate);
schemas.examinationTables.plugin(mongooseAggregatePaginate);
schemas.registrationToExaminationTable.plugin(mongooseAggregatePaginate);
schemas.stockOfAmbos.plugin(mongooseAggregatePaginate);
schemas.deliveryOfAmbos.plugin(mongooseAggregatePaginate);

module.exports = function(app) {

    var db_name = app.get('dbname');
    var mongodb_connection_string = 'mongodb://localhost/' + db_name;

    mongoose.Promise = global.Promise;
    mongoose.connect(mongodb_connection_string, { useMongoClient: true });

    global.db = {
        User : mongoose.model('user', schemas.userSchema),
        Role : mongoose.model('role', schemas.roleSchema),
        Permission : mongoose.model('permission', schemas.permissionSchema),
        Career: mongoose.model('career', schemas.careerSchema),
        StudentFile : mongoose.model('student_file', schemas.studentFileSchema),
        Config: mongoose.model('config', schemas.configSchema),
        Log: mongoose.model('log', schemas.logSchema),
        RegistrationToCareer: mongoose.model('registration_to_career', schemas.registrationToCareerSchema),
        Payment: mongoose.model('payment', schemas.payments),
        PaymentRulesForFee: mongoose.model('payment_rules_for_fee', schemas.paymentRulesForFee),
        PaymentRulesForTuition: mongoose.model('payment_rules_for_tuition', schemas.paymentRulesForTuition),
        ExaminationTables: mongoose.model('examination_table', schemas.examinationTables),
        RegistrationToExaminationTables: mongoose.model('registration_to_examination_table', schemas.registrationToExaminationTable),
        DeliveryOfAmbos: mongoose.model('delivery_of_ambos', schemas.deliveryOfAmbos),
        StockOfAmbos: mongoose.model('stock_of_ambos', schemas.stockOfAmbos)
    }
};