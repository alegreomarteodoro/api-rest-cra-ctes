var express = require('express');
var path = require('path');
var passport = require('passport');
var methodOverride = require('method-override');
var logger = require('morgan');
var bodyParser = require('body-parser');

module.exports = function(app) {

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json({ type: 'application/json'}));

    app.use(logger('dev'));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use("/public", express.static(path.resolve('./public')));
    app.use(methodOverride());

    app.disable('x-powered-by');

};