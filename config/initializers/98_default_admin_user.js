var mongoose = require('mongoose');

module.exports = function(app) {

    //check if default Admin User exits
    db.User.findOne({admin: true}, function (err, user) {
        if(!user) {
            var email = app.get('defaultAdminUser');
            var pass = app.get('defaultAdminPassword');

            db.User.register({email: email}, pass, function(err, user) {
                if (err) {
                    console.log(err);
                } else {
                    user.admin = true;
                    user.save();
                }
            });
        };
    });

};