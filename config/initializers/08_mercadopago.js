var mercadopago = require('mercadopago');

module.exports = function(app) {

    var args = {
        client_id: app.get('mercadopago.clientID'),
        client_secret: app.get('mercadopago.clientSecret'),
    };

    if ('development' == app.get('env')) {
        args = {
            sandbox: true,
            access_token: app.get('mercadopago.accessToken')
        }
    }

    mercadopago.configure(args);

    /*
    //version vieja de mercado pago
    var mp = new mercadopago(app.get('mercadopago.clientID'), app.get('mercadopago.clientSecret'));
    if ('development' == app.get('env')) {
        mp.sandboxMode(true);
    }

    app.set('mercadopago', mp);
    */

    app.set('mercadopago', mercadopago);
};