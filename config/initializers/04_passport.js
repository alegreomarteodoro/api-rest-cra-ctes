var passport    = require('passport');
var LocalStrategy = require('passport-local').Strategy;

module.exports = function(app) {

    // use static authenticate method of model in LocalStrategy
    passport.use(new LocalStrategy(db.User.authenticate()));

    // use static serialize and deserialize of model for passport session support
    passport.serializeUser(db.User.serializeUser());
    passport.deserializeUser(db.User.deserializeUser());

};