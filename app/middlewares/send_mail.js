var nodemailer = require('nodemailer');
var app = require('../../server');

module.exports = function(to, subject, html, bcc) {

    var transporter = nodemailer.createTransport({
            host: app.get('smtp.host'),
            port: app.get('smtp.port'),
            secure: true,
            auth: {
                user: app.get('smtp.user'),
                pass: app.get('smtp.pass')
            },
            tls:{
                rejectUnauthorized: false
            }
        });

    var mailOptions = {
        from: app.get('smtp.from'),
        to: to,
        subject: subject,
        html: html
    };

    if(bcc) {
        mailOptions.bcc = bcc;
    }

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return console.log(error);
        }
    });
}