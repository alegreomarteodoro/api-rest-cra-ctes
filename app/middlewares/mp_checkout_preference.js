var q = require('q');
var app = require('../../server');

module.exports = {

    // params.title
    // params.picture_url
    // params.description
    // params.price
    // params.user
    // params.payment_id

    regular_payment: function (params) {

        var defer = q.defer();

        var price = params.price;
        var description = params.description;

        defer.resolve({
            "items": [{
                "id": params.payment_id,
                "title": params.title,
                "currency_id": "ARS",
                "picture_url": params.picture_url,
                "description": description,
                "quantity": 1,
                "unit_price": price
            }],
            "payer": {
                "email": params.user.email,
                "name": params.user.first_name,
                "surname": params.user.last_name,
                "identification": {
                    "type" : 'DNI',
                    "number": params.user.dni
                }
            },
            "external_reference": params.payment_id,
            "back_urls": {
                'success': app.get('mercadopago.backUrls.success'),
                'pending': app.get('mercadopago.backUrls.pending'),
                'failure': app.get('mercadopago.backUrls.failure')
            },
            "auto_return": app.get('mercadopago.autoReturn')
        });

        return defer.promise;
    }
};