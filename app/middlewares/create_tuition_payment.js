var mongoose = require('mongoose');

module.exports = function( registrationToCareer ) {

    // Traemos el ciclo lectivo actual

    db.Config.findOne({ 'key': 'registrationToCareers.academicYear' }, function (err, config) {

        if (err) return res.json({
            success: false,
            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
        });

        var currentYear = config.value;
        var currentMonth = 1;

        // Chequeamos que el ciclo lectivo de la inscripcion sea el mismo que el de la configuracion

        if ( currentYear === registrationToCareer.academicYear ) {

            db.Payment.findOne({
                'year': currentYear,
                'month': currentMonth,
                'user': registrationToCareer.user
            }, function (err, payment) {

                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if (!payment) {

                    db.PaymentRulesForTuition.findOne({
                        career: registrationToCareer.career,
                        active: true
                    }, function (err, paymentRule) {

                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        if (paymentRule) {

                            payment = new db.Payment({
                                _id: new mongoose.Types.ObjectId(),
                                user: registrationToCareer.user,
                                title: 'Matrícula ' + currentYear,
                                paymentStatus: 'pending',
                                paymentType: 'enrollment',
                                amount: paymentRule.baseAmount, // importe a pagar
                                amountPaid: 0, // importe pagado
                                month: currentMonth,
                                year: currentYear,
                                dueDate: paymentRule.expirationDay
                            });

                            payment.save();

                        }

                    });
                }
            });

        }

    });

};