var mongoose = require('mongoose');

var ConfigSchema = new mongoose.Schema({
    key: String,
    value: String,
    dataType: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now }
});

module.exports = ConfigSchema;