var mongoose = require('mongoose');

var RegistrationToCareerSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' }, // usuario con rol alumno
    career: { type: mongoose.Schema.Types.ObjectId, ref: 'career' }, // carrera
    level: String, // año o nivel que cursa
    academicYear: String, // ciclo lectivo,
    repeatingTheClass: { // recursante
        type: Boolean,
        default: false
    },
    createdAt : { type: Date, default: Date.now }, // fecha de inscripción a la carrera
    lastModified : { type: Date, default: Date.now },
    progress: { // progreso en la inscripcion //TODO: esto es temporal, cuando este terminado y migrado el campo de estados eliminar esto
        step1Completed: { // formulario con carrera completo
            type: Boolean,
            default: false
        },
        step2Completed: { // formulario con nivel completo
            type: Boolean,
            default: false
        },
        step3Completed: { // formulario con datos personales completo
            type: Boolean,
            default: false
        },
        approved: {
            finalApproved: { // inscripcion aprobada
                type: Boolean,
                default: false
            },
            conditionalApproved: { // inscripcion aprobada de manera condicional
                type: Boolean,
                default: false
            }
        },
        enrolled: { // alumno matriculado, es decir que pago la matricula
            type: Boolean,
            default: false
        }
    },
    observations: String, // Observaciones dejadas por el debel durante la aprobación
    studentFile: { type: mongoose.Schema.Types.ObjectId, ref: 'student_file' },
    payment: { //TODO: esto es temporal hasta desarrollar el modulo de pagos
        receiptNumber: String
    },
    studentRegistrationStatus: { // estado de inscripcion del alumno - reemplaza al campo progress
        type: String,
        default: 'pending'
    },
    studyPlan:  { type: mongoose.Schema.Types.ObjectId }
});

module.exports = RegistrationToCareerSchema;