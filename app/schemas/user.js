var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var passportLocalMongoose = require('passport-local-mongoose');
var q = require('q');

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password : String,
    displayName : String,
    avatar: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    active : { type: Boolean, default: true },
    role: { type: mongoose.Schema.Types.ObjectId, ref: 'role' },
    admin: { type: Boolean, default: false },
    activationCode: String,
    verifiedAccount: { type: Boolean, default: false }
});

UserSchema.plugin(passportLocalMongoose, {usernameField: 'email'});

// generating a hash
UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// password verification
UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

UserSchema.statics.checkEmail = function (username) {
    var defer = q.defer();

    this.findOne({
        username: username
    }, function(err, user) {
        if (err) {
            defer.reject(err);
        } else {
            defer.resolve(user);
        }
    });

    return defer.promise;
};

module.exports = UserSchema;
