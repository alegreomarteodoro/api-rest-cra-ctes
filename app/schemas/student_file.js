var mongoose = require('mongoose');

var StudentFileSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    currentCareer: { type: mongoose.Schema.Types.ObjectId, ref: 'career' },
    currentLevel: String,
    currentStudyPlan: { type: mongoose.Schema.Types.ObjectId },
	personalData: {
        lastName: {
	        type: String,
	        uppercase: true
	    },
        firstName: {
	        type: String,
            uppercase: true
	    },
        email: {
            type: String,
            lowercase: true
        },
        dni: Number,
        gender:  String,
        cuil: Number,
        size: String,
        birthDate: { type: Date },
        birthPlace: {
            locality: String,
            province: String,
            nationality: String
        },
        currentResidence: {
            street: String,
            streetNumber: Number,
            floor:  Number,
            departmentNumber: String,
            neighborhood: String,
            province:String,
            country: String,
            zipCode:Number,
            phone: Number,
            cellPhone: Number,
            cellPhoneCompany: String,
            locality:String
        }
	},
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now }
});

module.exports = StudentFileSchema;