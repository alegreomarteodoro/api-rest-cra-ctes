var mongoose = require('mongoose');

var RegistrationToExaminationTableSchema = new mongoose.Schema({
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' }, // alumno
    subjects: [{ type: mongoose.Schema.Types.ObjectId }], // materias en las que se inscribe
    career: { type: mongoose.Schema.Types.ObjectId, ref: 'career' }, // carrera
    examinationTable: { type: mongoose.Schema.Types.ObjectId, ref: 'examination_table' } // mesa de examen
});

module.exports = RegistrationToExaminationTableSchema;