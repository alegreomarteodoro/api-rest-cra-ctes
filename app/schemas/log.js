var mongoose = require('mongoose');

var LogSchema = new mongoose.Schema({
    title: String,
    description: String,
    createdAt : { type: Date, default: Date.now },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' }
});

module.exports = LogSchema;