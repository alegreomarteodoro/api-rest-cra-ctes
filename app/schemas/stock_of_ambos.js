var mongoose = require('mongoose');

var CRAProductsSchema = new mongoose.Schema({
    name: { // nombre del producto
        type: String,
        uppercase: true
    },
    description: String, // descripcion
    career: { type: mongoose.Schema.Types.ObjectId, ref: 'career' }, // carrera
    price: Number, // precio
    amount: Number, // cantidad
    size: String, // talle
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    active : { type: Boolean, default: true }
});

module.exports = CRAProductsSchema;