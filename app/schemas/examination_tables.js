var mongoose = require('mongoose');

var ExaminationTablesSchema = new mongoose.Schema({
    name: {
        type: String,
        uppercase: true
    },
    description: String,
    dateFrom : { type: Date, default: Date.now }, //fecha desde para la inscripcion a la mesa
    dateTo : { type: Date, default: Date.now }, //fecha hasta para la inscripcion a la mesa
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    active : { type: Boolean, default: true },
    careers: [{ type: mongoose.Schema.Types.ObjectId, ref: 'career' }], // carreras
    publishListsFrom: { type: Date }, // fecha desde para la publicacion de listas de la mesa
    publishListsTo: { type: Date }, // fecha hasta para la publicacion de listas de la mesa
    examsFrom: { type: Date }, // fecha desde para los examenes
    examsTo:  { type: Date }, // fecha hasta para los examenes
    called: { // llamado
        type: String,
        uppercase: true
    },
    monthOfTheLastPaymentRequired: Number, // Mes de la ultima cuota paga requerida
    yearOfTheLastPaymentRequired: Number // Año de la ultima cuota paga requerida
});

module.exports = ExaminationTablesSchema;