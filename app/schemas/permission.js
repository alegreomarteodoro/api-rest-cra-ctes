var mongoose = require('mongoose');

var PermissionSchema = new mongoose.Schema({
    name: String,
    key: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    module: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now }
});

module.exports = PermissionSchema;