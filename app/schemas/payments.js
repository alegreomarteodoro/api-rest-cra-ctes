var mongoose = require('mongoose');

var PaymentSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    title: String, // titulo
    description: String, // descripcion de la operacion
    receiptNumber: String, // numero de recibo del sistema de cobros de tesoreria
    paymentMethod: String, // metodo de pago: mercadopago, cash.
    paymentType: String, // tipo de pago: fee, enrollment
    paymentStatus: String, // estado del pago: pending; approved; authorized; in_process; in_mediation; rejected; cancelled; refunded; charged_back ;
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    amount: Number, // importe a pagar
    amountPaid: Number, // importe pagado
    paymentDate: { type: Date }, // fecha de pago
    dueDate: { type: Date }, // fecha de vencimiento
    month: Number, // mes
    year: Number, // año
    mercadopagoPaymentId: String
});

module.exports = PaymentSchema;