var mongoose = require('mongoose');

var RoleSchema = new mongoose.Schema({
    name: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    active : { type: Boolean, default: true },
    permissions: [{ type: mongoose.Schema.Types.ObjectId, ref: 'permission' }],
    isDefault : { type: Boolean, default: false }
});

module.exports = RoleSchema;