var mongoose = require('mongoose');

var PaymentRulesForFeeSchema = new mongoose.Schema({
    career: { type: mongoose.Schema.Types.ObjectId, ref: 'career' },
    name: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    active : { type: Boolean, default: true },
    baseAmount: { type: Number, default: 0 }, // monto base
    discount: {
        untilDay: { type: Number, default: 0 }, // hasta el dia
        amount: { type: Number, default: 0 } // monto a descontar
    },
    expirationDay: { type: Number, default: 0 }, // dia del vencimiento de la cuota
    interests: [{
        daysOfDelay: Number, // dias de retraso
        amount: Number // monto de interes
    }]
});

module.exports = PaymentRulesForFeeSchema;