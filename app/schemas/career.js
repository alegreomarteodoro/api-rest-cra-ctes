var mongoose = require('mongoose');

var CareerSchema = new mongoose.Schema({
    name: String,
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    active : { type: Boolean, default: true },

    studyPlans: [{ //Planes de estudio
        resolution: String,             // numero de resolucion
        content: String,                // contenido de la resolucion
        resolutionDate: { type: Date }, // fecha de la resolucion
        fileNumber: String,            // numero de expediente
        createdAt : { type: Date, default: Date.now },
        lastModified : { type: Date, default: Date.now },
        active : { type: Boolean, default: true },
        isDefault: { type: Boolean, default: false },

        levels: [{ //Años
            name: String,
            value: Number,
            createdAt : { type: Date, default: Date.now },
            lastModified : { type: Date, default: Date.now },
            active : { type: Boolean, default: true },

            subjects: [{ //Materias
                curricularSpace: String,
                duration: String,
                weeklyHours: Number,
                createdAt : { type: Date, default: Date.now },
                lastModified : { type: Date, default: Date.now },
                active : { type: Boolean, default: true }
            }]

        }]    // años de la carrera, por ejemplo: 1ro, 2do, 3ro...

    }]

});

module.exports = CareerSchema;