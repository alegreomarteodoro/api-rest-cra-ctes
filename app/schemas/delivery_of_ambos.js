var mongoose = require('mongoose');

var DeliveryOfProductsSchema = new mongoose.Schema({
    deliveredToUser: { type: mongoose.Schema.Types.ObjectId, ref: 'user' }, //entregado al usuario
    deliveredBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user' }, //entregado por el usuario
    deliveredProduct: { type: mongoose.Schema.Types.ObjectId, ref: 'stock_of_ambos' }, // producto entregado
    deliveredQuantity: Number, // cantidad entregada
    comments: String, // comentarios
    relatedPayment: { type: mongoose.Schema.Types.ObjectId, ref: 'payment' },
    createdAt : { type: Date, default: Date.now },
    lastModified : { type: Date, default: Date.now },
    status: String,
    active : { type: Boolean, default: true }
});

module.exports = DeliveryOfProductsSchema;