var mongoose = require('mongoose');

module.exports = {

    list : function (req, res) {
        db.Subject
            .find()
            .exec(function (error, subjects) {
                if (error) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    data: subjects,
                    success: true
                });
            });
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;

        var subject = new db.Subject({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            active: active
        });

        subject.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'Los datos se han guardado exitosamente.'
            });
        });
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;

        db.Subject.findById(id, function (err, subject) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            subject.name = name;
            subject.active = active;
            subject.lastModified = Date.now();

            subject.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'Los datos se han guardado exitosamente.'
                });
            });
        });
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Subject.remove({'_id': id}, function (error, cmd) {
            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if(cmd.result.ok && cmd.result.n > 0) {
                return res.json({
                    success: true,
                    data: {
                        _id: id
                    },
                    message: 'La operación se ha realizado con éxito.'
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No se ha encontrado el registro que desea eliminar.'
                });
            }
        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Subject.findById(id, function (err, subject) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: subject,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};