var mongoose = require('mongoose');

module.exports = {

    list: function (req, res) {
        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;

        var aggregate = db.ExaminationTables.aggregate();
        aggregate.lookup({
            "from": "careers",
            "localField": "careers",
            "foreignField": "_id",
            "as": "careersObjects"
        });
        aggregate.unwind({
            "path": '$careersObjects'
        });
        aggregate.group({
            "_id": "$_id",
            "name":{ "$first": "$name" },
            "dateFrom":{ "$first": "$dateFrom" },
            "dateTo":{ "$first": "$dateTo" },
            "active":{ "$first": "$active" },
            "createdAt":{ "$first": "$createdAt" },
            "lastModified":{ "$first": "$lastModified" },
            "careers": { "$push": "$careersObjects" },
            "called":{ "$first": "$called" }
        });

        aggregate.sort({ 'dateFrom': -1 });


        var options = {
            page: Number(page),
            limit: Number(limit)
        };

        db.ExaminationTables
            .aggregatePaginate(aggregate, options, function (error, examinationTables, pageCount, count) {
                if (error) return res.json({
                    sucess: false,
                    data: {
                        docs: [],
                        total: 0,
                        pages: 0,
                        page: 0
                    },
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: {
                        docs: examinationTables,
                        total: count,
                        page: page,
                        pages: pageCount
                    },
                    success: true,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },



    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        /*db.StudentFile.findOne({ user: id }, function(err, studentFile) {

            if (err) return res.json({
                success: false,
                data: err,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if(studentFile) {

                return res.json({
                    success: false,
                    message: 'No se puede eliminar el registro porque tiene datos asociados a otros modulos.'
                });

            } else {

                db.User.remove({'_id': id}, function (error, cmd) {
                    if (error) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if(cmd.result.ok && cmd.result.n > 0) {
                        return res.json({
                            success: true,
                            data: {
                                _id: id
                            },
                            message: 'La operación se ha realizado con éxito.'
                        });
                    } else {
                        return res.json({
                            success: false,
                            message: 'No se ha encontrado el registro que desea eliminar.'
                        });
                    }
                });

            }


        });*/
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var description = req.body.description || req.query.description || req.params.description;
        var dateFrom = new Date((req.body.dateFrom || req.query.dateFrom || req.params.dateFrom) + 'T01:00:00.000Z');
        var dateTo = new Date((req.body.dateTo || req.query.dateTo || req.params.dateTo) + 'T23:59:59.999Z');
        var active = req.body.active || req.query.active || req.params.active || false;
        var careers = req.body.careers || req.query.careers || req.params.careers;
        var publishListsFrom = req.body.publishListsFrom || req.query.publishListsFrom || req.params.publishListsFrom;
        var publishListsTo = req.body.publishListsTo || req.query.publishListsTo || req.params.publishListsTo;
        var examsFrom = req.body.examsFrom || req.query.examsFrom || req.params.examsFrom;
        var examsTo = req.body.examsTo || req.query.examsTo || req.params.examsTo;
        var called = req.body.called || req.query.called || req.params.called;
        var monthOfTheLastPaymentRequired = req.body.monthOfTheLastPaymentRequired || req.query.monthOfTheLastPaymentRequired || req.params.monthOfTheLastPaymentRequired;
        var yearOfTheLastPaymentRequired = req.body.yearOfTheLastPaymentRequired || req.query.yearOfTheLastPaymentRequired || req.params.yearOfTheLastPaymentRequired;

        if(publishListsFrom) publishListsFrom = new Date(publishListsFrom + 'T01:00:00.000Z');
        if(publishListsTo) publishListsTo = new Date(publishListsTo + 'T23:59:59.999Z');
        if(examsFrom) examsFrom = new Date(examsFrom + 'T01:00:00.000Z');
        if(examsTo) examsTo = new Date(examsTo + 'T23:59:59.999Z');

        var examinationTable = new db.ExaminationTables({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            active: active,
            dateFrom: dateFrom,
            dateTo: dateTo,
            careers: careers,
            description: description,
            publishListsFrom: publishListsFrom,
            publishListsTo: publishListsTo,
            examsFrom: examsFrom,
            examsTo: examsTo,
            called: called,
            monthOfTheLastPaymentRequired: monthOfTheLastPaymentRequired,
            yearOfTheLastPaymentRequired: yearOfTheLastPaymentRequired
        });

        examinationTable.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'Los datos se han guardado exitosamente.'
            });
        });
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var description = req.body.description || req.query.description || req.params.description;
        var dateFrom = new Date((req.body.dateFrom || req.query.dateFrom || req.params.dateFrom) + 'T01:00:00.000Z');
        var dateTo = new Date((req.body.dateTo || req.query.dateTo || req.params.dateTo) + 'T23:59:59.999Z');
        var active = req.body.active || req.query.active || req.params.active || false;
        var careers = req.body.careers || req.query.careers || req.params.careers;
        var publishListsFrom = req.body.publishListsFrom || req.query.publishListsFrom || req.params.publishListsFrom;
        var publishListsTo = req.body.publishListsTo || req.query.publishListsTo || req.params.publishListsTo;
        var examsFrom = req.body.examsFrom || req.query.examsFrom || req.params.examsFrom;
        var examsTo = req.body.examsTo || req.query.examsTo || req.params.examsTo;
        var called = req.body.called || req.query.called || req.params.called;
        var monthOfTheLastPaymentRequired = req.body.monthOfTheLastPaymentRequired || req.query.monthOfTheLastPaymentRequired || req.params.monthOfTheLastPaymentRequired;
        var yearOfTheLastPaymentRequired = req.body.yearOfTheLastPaymentRequired || req.query.yearOfTheLastPaymentRequired || req.params.yearOfTheLastPaymentRequired;

        if(publishListsFrom) publishListsFrom = new Date(publishListsFrom + 'T01:00:00.000Z');
        if(publishListsTo) publishListsTo = new Date(publishListsTo + 'T23:59:59.999Z');
        if(examsFrom) examsFrom = new Date(examsFrom + 'T01:00:00.000Z');
        if(examsTo) examsTo = new Date(examsTo + 'T23:59:59.999Z');

        db.ExaminationTables.findById(id, function (err, examinationTable) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            examinationTable.name = name;
            examinationTable.active = active;
            examinationTable.lastModified = Date.now();
            examinationTable.dateFrom = dateFrom;
            examinationTable.dateTo = dateTo;
            examinationTable.careers = careers;
            examinationTable.description = description;
            examinationTable.publishListsFrom = publishListsFrom;
            examinationTable.publishListsTo = publishListsTo;
            examinationTable.examsFrom = examsFrom;
            examinationTable.examsTo = examsTo;
            examinationTable.called = called;
            examinationTable.monthOfTheLastPaymentRequired = monthOfTheLastPaymentRequired;
            examinationTable.yearOfTheLastPaymentRequired = yearOfTheLastPaymentRequired;

            examinationTable.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'Los datos se han guardado exitosamente.'
                });
            });
        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.ExaminationTables.findById(id)
            .exec(function (err, examinationTable) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    success: true,
                    data: examinationTable,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    }

};