var mongoose = require('mongoose');

module.exports = {

    list : function (req, res) {
        db.Permission
            .find()
            .exec(function (error, roles) {
                if (error) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    data: roles,
                    success: true
                });
            });
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var key = req.body.key || req.query.key || req.params.key;
        var module = req.body.module || req.query.module || req.params.module;

        var role = new db.Permission({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            key: key,
            module: module
        });

        role.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'El permiso se ha creado exitosamente.'
            });
        });
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var key = req.body.key || req.query.key || req.params.key;
        var module = req.body.module || req.query.module || req.params.module;

        db.Permission.findById(id, function (err, item) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            item.name = name;
            item.key = key;
            item.module = module;
            item.lastModified = Date.now();

            item.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'El permiso se ha creado exitosamente.'
                });
            });
        });
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Permission.remove({'_id': id}, function (error, cmd) {
            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if(cmd.result.ok && cmd.result.n > 0) {
                return res.json({
                    success: true,
                    data: {
                        _id: id
                    },
                    message: 'La operación se ha realizado con éxito.'
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No se ha encontrado el registro que desea eliminar.'
                });
            }
        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Permission.findById(id, function (err, item) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: item,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};