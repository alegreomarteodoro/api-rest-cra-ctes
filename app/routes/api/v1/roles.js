var mongoose = require('mongoose');

module.exports = {

    list : function (req, res) {
        db.Role
            .find()
            .exec(function (error, roles) {
                if (error) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    data: roles,
                    success: true
                });
            });
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;
        var permissions = req.body.permissions || req.query.permissions || req.params.permissions;
        var isDefault = req.body.isDefault || req.query.isDefault || req.params.isDefault || false;

        var role = new db.Role({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            active: active,
            permissions: permissions
        });

        if(isDefault) {

            db.Role.findOne({isDefault: true}, function (err, item) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if (item) {

                    item.isDefault = false;
                    item.save(function (err, item) {

                        role.isDefault = true;

                        role.save(function (err, item_saved) {
                            if (err) return res.json({
                                success: false,
                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                            });

                            return res.json({
                                success: true,
                                data: item_saved,
                                message: 'El rol se ha creado exitosamente.'
                            });
                        });
                    });

                } else {

                    role.isDefault = true;

                    role.save(function (err, item_saved) {
                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });
                        return res.json({
                            success: true,
                            data: item_saved,
                            message: 'El rol se ha creado exitosamente.'
                        });
                    });
                }
            });

        } else {

            role.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'El rol se ha creado exitosamente.'
                });
            });

        }
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;
        var permissions = req.body.permissions || req.query.permissions || req.params.permissions;
        var isDefault = req.body.isDefault || req.query.isDefault || req.params.isDefault || false;

        db.Role.findById(id, function (err, role) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            role.name = name;
            role.active = active;
            role.lastModified = Date.now();
            role.permissions = permissions;

            if (isDefault) {

                db.Role.findOne({isDefault: true}, function (err, item) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if (item && !item._id.equals(role._id)) {

                        item.isDefault = false;

                        item.save(function (err, item) {

                            role.isDefault = true;

                            role.save(function (err, item_saved) {
                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });
                                return res.json({
                                    success: true,
                                    data: item_saved,
                                    message: 'El rol se ha guardado exitosamente.'
                                });
                            });
                        });
                    } else {

                        role.isDefault = true;

                        role.save(function (err, item_saved) {
                            if (err) return res.json({
                                success: false,
                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                            });
                            return res.json({
                                success: true,
                                data: item_saved,
                                message: 'El rol se ha guardado exitosamente.'
                            });
                        });
                    }
                });
            } else {

                role.save(function (err, item_saved) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });
                    return res.json({
                        success: true,
                        data: item_saved,
                        message: 'El rol se ha guardado exitosamente.'
                    });
                });

            }
        });
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;


        db.User.findOne({ role: id }, function(err, role) {

            if (err) return res.json({
                success: false,
                data: err,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if (role) {

                return res.json({
                    success: false,
                    message: 'No se puede eliminar el registro porque tiene datos asociados a otros modulos.'
                });

            } else {

                db.Role.remove({'_id': id}, function (error, cmd) {
                    if (error) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if (cmd.result.ok && cmd.result.n > 0) {
                        return res.json({
                            success: true,
                            data: {
                                _id: id
                            },
                            message: 'La operación se ha realizado con éxito.'
                        });
                    } else {
                        return res.json({
                            success: false,
                            message: 'No se ha encontrado el registro que desea eliminar.'
                        });
                    }
                });
            }

        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Role.findById(id, function (err, role) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: role,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};