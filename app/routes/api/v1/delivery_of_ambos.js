var mongoose = require('mongoose');

module.exports = {

    list: function (req, res) {
        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;
        var dni = req.body.dni || req.query.dni || req.params.dni;
        var career = req.body.career || req.query.career || req.params.career;
        var size = req.body.size || req.query.size || req.params.size;

        var aggregate = db.DeliveryOfAmbos.aggregate();
        aggregate.lookup({
            "from": "student_files",
            "localField": "deliveredToUser",
            "foreignField": "user",
            "as": "file"
        });
        aggregate.unwind('$file');

        aggregate.lookup({
            "from": "stock_of_ambos",
            "localField": "deliveredProduct",
            "foreignField": "_id",
            "as": "deliveredProduct"
        });
        aggregate.unwind('$deliveredProduct');

        aggregate.lookup({
            "from": "careers",
            "localField": "deliveredProduct.career",
            "foreignField": "_id",
            "as": "deliveredProduct.career"
        });
        aggregate.unwind('$deliveredProduct.career');

        if(dni) {
            aggregate.match({'file.personalData.dni': {'$eq': Number(dni)}});
        }

        if(career) {
            aggregate.match({'deliveredProduct.career._id': {'$eq': mongoose.Types.ObjectId(career)}});
        }

        if(size) {
            aggregate.match({'deliveredProduct.size': {'$eq': size}});
        }

        var options = {
            sort: { 'createdAt' : -1 },
            page: Number(page),
            limit: Number(limit)
        };

        db.DeliveryOfAmbos
            .aggregatePaginate(aggregate, options, function (error, items, pageCount, count) {
                if (error) return res.json({
                    sucess: false,
                    data: {
                        docs: [],
                        total: 0,
                        pages: 0,
                        page: 0
                    },
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: {
                        docs: items,
                        total: count,
                        page: page,
                        pages: pageCount
                    },
                    success: true,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },

    create : function (req, res) {
        var deliveredToUser = req.body.deliveredToUser || req.query.deliveredToUser || req.params.deliveredToUser;
        var deliveredProduct = req.body.deliveredProduct || req.query.deliveredProduct || req.params.deliveredProduct;
        var deliveredQuantity = req.body.deliveredQuantity || req.query.deliveredQuantity || req.params.deliveredQuantity || 0;
        var comments = req.body.comments || req.query.comments || req.params.comments;
        var status = req.body.status || req.query.status || req.params.status;

        var productId = deliveredProduct.value;
        var userId = deliveredToUser.value;
        deliveredQuantity = Number(deliveredQuantity);

        db.StockOfAmbos.findById(productId, function (err, product) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if(product && product.amount >= deliveredQuantity) {

                var newItem = new db.DeliveryOfAmbos({
                    _id: new mongoose.Types.ObjectId(),
                    deliveredToUser: userId,
                    deliveredProduct: productId,
                    status: status,
                    deliveredQuantity: deliveredQuantity,
                    comments: comments
                });

                newItem.save(function (err, item_saved) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    product.amount = product.amount - deliveredQuantity;
                    product.save();

                    return res.json({
                        success: true,
                        data: item_saved,
                        message: 'El registro se ha creado exitosamente.'
                    });
                });

            } else {
                return res.json({
                    success: false,
                    message: 'No hay stock suficiente de la prenda que desea entregar.'
                });
            }
        });

    },

    edit : function (req, res) {
        var deliveredToUser = req.body.deliveredToUser || req.query.deliveredToUser || req.params.deliveredToUser;
        var deliveredProduct = req.body.deliveredProduct || req.query.deliveredProduct || req.params.deliveredProduct;
        var deliveredQuantity = req.body.deliveredQuantity || req.query.deliveredQuantity || req.params.deliveredQuantity || 0;
        var comments = req.body.comments || req.query.comments || req.params.comments;
        var status = req.body.status || req.query.status || req.params.status;

        db.DeliveryOfAmbos.findById(id, function (err, item) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            item.deliveredToUser = deliveredToUser;
            item.lastModified = Date.now();
            item.deliveredProduct = deliveredProduct;
            item.deliveredQuantity = deliveredQuantity;
            item.comments = comments;
            item.status = status;

            item.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'El registro se ha guardado exitosamente.'
                });
            });

        });
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id; //id de entrega a eliminar
        var recoverStock = req.body.recoverStock || req.query.recoverStock || req.params.recoverStock; //recuperar stock

        db.DeliveryOfAmbos.findById(id, function (error, item) {

            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if( item ) {
                var deliveredQuantity = item.deliveredQuantity;
                var productId = item.deliveredProduct;

                db.StockOfAmbos.findById(productId, function (error, itemStock) {

                    if (error) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if(itemStock && recoverStock === 'true') {
                        itemStock.amount = itemStock.amount + deliveredQuantity;
                        itemStock.save();
                    }

                    db.DeliveryOfAmbos.remove({'_id': id}, function (error, cmd) {
                        if (error) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        if (cmd.result.ok && cmd.result.n > 0) {
                            return res.json({
                                success: true,
                                data: {
                                    _id: id
                                },
                                message: 'La operación se ha realizado con éxito.'
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'No se ha encontrado el registro que desea eliminar.'
                            });
                        }
                    });

                });
            }
        });

    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.DeliveryOfAmbos.findById(id, function (err, item) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: item,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};