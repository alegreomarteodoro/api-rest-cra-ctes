var mongoose = require('mongoose');

module.exports = {

    list : function (req, res) {
        db.Config
            .find()
            .exec(function (error, configs) {
                if (error) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    data: configs,
                    success: true
                });
            });
    },

    edit : function (req, res) {
        var key = req.body.key || req.query.key || req.params.key;
        var value = req.body.value || req.query.value || req.params.value;
        var dataType = req.body.dataType || req.query.dataType || req.params.dataType;

        db.Config.findOne({ 'key': key }, function (err, config) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if(config) {

                config.value = value;

            } else {

                config = new db.Config({
                    _id: new mongoose.Types.ObjectId(),
                    key: key,
                    value: value,
                    dataType: dataType
                });

            }

            config.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'Los datos se han guardado exitosamente.'
                });
            });
        });
    },

    get : function (req, res) {
        var key = req.body.key || req.query.key || req.params.key;

        db.Config.findOne({ 'key': key }, function (err, config) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: config,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};