var mongoose = require('mongoose');

module.exports = {
    list : function (req, res) {
        db.Career
            .find().populate([{path:'paymentRulesToEnrollment'},{path:'paymentRulesToQuota'}])
            .exec(function (error, careers) {
                if (error) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    data: careers,
                    success: true
                });
            });
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;
        var studyPlans = req.body.studyPlans || req.query.studyPlans || req.params.studyPlans;

        var career = new db.Career({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            active: active,
            studyPlans: studyPlans
        });

        career.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'Los datos se han guardado exitosamente.'
            });
        });
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;
        var studyPlans = req.body.studyPlans || req.query.studyPlans || req.params.studyPlans;

        db.Career.findById(id, function (err, career) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            career.name = name;
            career.active = active;
            career.lastModified = Date.now();
            if(studyPlans) career.studyPlans = studyPlans;

            career.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'Los datos se han guardado exitosamente.'
                });
            });
        });
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.RegistrationToCareer.findOne({ career: id }, function(err, career) {

            if (err) return res.json({
                success: false,
                data: err,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if (career) {

                return res.json({
                    success: false,
                    message: 'No se puede eliminar el registro porque tiene datos asociados a otros modulos.'
                });

            } else {

                db.Career.remove({'_id': id}, function (error, cmd) {
                    if (error) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if (cmd.result.ok && cmd.result.n > 0) {
                        return res.json({
                            success: true,
                            data: {
                                _id: id
                            },
                            message: 'La operación se ha realizado con éxito.'
                        });
                    } else {
                        return res.json({
                            success: false,
                            message: 'No se ha encontrado el registro que desea eliminar.'
                        });
                    }
                });

            }
        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Career.findOne({ _id: id }).populate([{path:'paymentRulesToEnrollment'},{path:'paymentRulesToQuota'}]).
        exec(function (err, career) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            if(career.paymentRulesToEnrollment && career.paymentRulesToEnrollment.increments){
                career.paymentRulesToEnrollment.increments.sort(function (a,b) {
                    return a.startingDate - b.startingDate
                });
            }
            return res.json({
                success: true,
                data: career,
                message: 'La operación se ha realizado con éxito.'
            });
        });

        /*db.Career.findById(id, function (err, career) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: career,
                message: 'La operación se ha realizado con éxito.'
            });
        });*/
    }
};