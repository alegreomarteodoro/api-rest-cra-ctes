var mongoose = require('mongoose');
var xlsx = require('excel4node');

module.exports = {

    get : function (req, res) {

        var currentUser = req.decoded ? req.decoded.data : null;
        var currentDate = new Date();

        if(currentUser) {

            db.RegistrationToCareer
                //.findOne({ user: currentUser._id, academicYear: currentDate.getFullYear(), 'progress.enrolled': true })
                // Por el momento se cambio para que se puedan inscribir los alumnos inscripciptos a una carrera con los estados aprobados o matriculados.
                .findOne({ user: currentUser._id, academicYear: currentDate.getFullYear(), $or: [{ 'studentRegistrationStatus': 'final-approved' }, { 'studentRegistrationStatus': 'conditional-approved' }, {'studentRegistrationStatus': 'enrolled-standard'}, {'studentRegistrationStatus': 'enrolled-for-exams'}, {'studentRegistrationStatus': 'enrolled-with-a-scholarship'}] })
                .populate('career')
                .exec(function (err, registrationToCareer) {

                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if(registrationToCareer && registrationToCareer.career) {

                        db.ExaminationTables.findOne({
                            'dateFrom': {
                                "$lte": currentDate
                            },
                            'dateTo': {
                                "$gte": currentDate
                            },
                            'active': true,
                            'careers': registrationToCareer.career._id
                            })
                            .exec(function (err, examinationTable) {
                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                if(examinationTable) {

                                    db.RegistrationToExaminationTables.findOne({
                                            examinationTable: examinationTable._id,
                                            user: currentUser._id,
                                            career: registrationToCareer.career._id
                                        })
                                        .exec(function (err, registrationToExaminationTable) {

                                            if (err) return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                            });

                                            if(examinationTable.monthOfTheLastPaymentRequired && examinationTable.yearOfTheLastPaymentRequired) {
                                                db.Payment.findOne({
                                                    'month': examinationTable.monthOfTheLastPaymentRequired,
                                                    'year': examinationTable.yearOfTheLastPaymentRequired,
                                                    'paymentType': 'fee',
                                                    'paymentStatus': 'approved',
                                                    'user': currentUser._id
                                                }, function (err, payment) {

                                                    if (err) return res.json({
                                                        success: false,
                                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                    });

                                                    if(payment) {
                                                        return res.json({
                                                            success: true,
                                                            data: {
                                                                examinationTable: examinationTable,
                                                                career: registrationToCareer.career,
                                                                registrationToExaminationTable: registrationToExaminationTable,
                                                                studyPlan: registrationToCareer.studyPlan
                                                            },
                                                            message: 'La operación se ha realizado con éxito.'
                                                        });
                                                    } else {
                                                        return res.json({
                                                            success: false,
                                                            message: 'Para poder inscribirte a las mesas de exámen debes tener pagada la cuota del mes '+ examinationTable.monthOfTheLastPaymentRequired + '/' + examinationTable.yearOfTheLastPaymentRequired + '.'
                                                        });
                                                    }

                                                });
                                            } else {

                                                return res.json({
                                                    success: true,
                                                    data: {
                                                        examinationTable: examinationTable,
                                                        career: registrationToCareer.career,
                                                        registrationToExaminationTable: registrationToExaminationTable,
                                                        studyPlan: registrationToCareer.studyPlan
                                                    },
                                                    message: 'La operación se ha realizado con éxito.'
                                                });
                                            }

                                        });

                                } else {

                                    return res.json({
                                        success: false,
                                        data: { },
                                        message: 'Actualmente no se están llevando a cabo las inscripciónes a las mesas de exámen.'
                                    });

                                }
                            });

                    } else {

                        return res.json({
                            success: false,
                            data: { },
                            message: 'Para poder inscribirte a las mesas de exámen primero debes actualizar tus datos en la sección Inscripción a Carreras en el menú de la izquiera.<br>Si ya actualizaste tus datos debes esperar por 30 o 40 minutos para poder inscribirte a las mesas.'
                        });

                    }

                });

        }
    },

    create : function (req, res) {
        var user = req.body.user || req.query.user || req.params.user;
        var subjects = req.body.subjects || req.query.subjects || req.params.subjects;
        var examinationTable = req.body.examinationTable || req.query.examinationTable || req.params.examinationTable;
        var career = req.body.career || req.query.career || req.params.career;


        var registrationToExaminationTable = new db.RegistrationToExaminationTables({
            _id: new mongoose.Types.ObjectId(),
            user: user,
            subjects: subjects,
            examinationTable: examinationTable,
            career: career
        });

        registrationToExaminationTable.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: item_saved,
                message: 'Los datos se han guardado exitosamente.'
            });
        });
    },


    edit : function (req, res) {
        var user = req.body.user || req.query.user || req.params.user;
        var subjects = req.body.subjects || req.query.subjects || req.params.subjects;
        var examinationTable = req.body.examinationTable || req.query.examinationTable || req.params.examinationTable;
        var career = req.body.career || req.query.career || req.params.career;

        db.RegistrationToExaminationTables
            .findOne({ 'user': user.value, 'examinationTable': examinationTable.value })
            .exec(function (err, registrationToExaminationTable) {

                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if (registrationToExaminationTable) {
                    registrationToExaminationTable.subjects = subjects;
                    registrationToExaminationTable.career = career;
                } else {
                    registrationToExaminationTable = new db.RegistrationToExaminationTables({
                        _id: new mongoose.Types.ObjectId(),
                        user: user.value,
                        subjects: subjects,
                        examinationTable: examinationTable.value,
                        career: career
                    });
                }

                registrationToExaminationTable.save(function (err, item_saved) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    return res.json({
                        success: true,
                        data: item_saved,
                        message: 'Los datos se han guardado exitosamente.'
                    });
                });

            });
    },

    list: function (req, res) {
        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;
        var examinationTable = req.body.examinationTable || req.query.examinationTable || req.params.examinationTable;
        var subject = req.body.subject || req.query.subject || req.params.subject;

        var aggregate = db.RegistrationToExaminationTables.aggregate();

        aggregate.lookup({
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user"
        });

        aggregate.unwind('$user');

        aggregate.lookup({
            "from": "student_files",
            "localField": "user._id",
            "foreignField": "user",
            "as": "file"
        });

        aggregate.unwind('$file');

        if(subject) {
            aggregate.match({'subjects': {'$eq': mongoose.Types.ObjectId(subject)}});
        }

        if(examinationTable) {
            aggregate.match({'examinationTable': {'$eq': mongoose.Types.ObjectId(examinationTable)}});
        }

        aggregate.group({
            "_id": "$user._id",
            "file":{ "$first": "$file" },
            "user":{ "$first": "$user" }
        });

        aggregate.sort({ 'file.personalData.lastName': 1 });

        var options = {
            page: Number(page),
            limit: Number(limit)
        };

        db.RegistrationToExaminationTables
            .aggregatePaginate(aggregate, options, function (error, resultData, pageCount, count) {
                if (error) return res.json({
                    sucess: false,
                    data: {
                        docs: [],
                        total: 0,
                        pages: 0,
                        page: 0
                    },
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: {
                        docs: resultData,
                        total: count,
                        page: page,
                        pages: pageCount
                    },
                    success: true,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },

    download: function (req, res) {
        var examinationTable = req.body.examinationTable || req.query.examinationTable || req.params.examinationTable;
        var subject = req.body.subject || req.query.subject || req.params.subject;
        var career = req.body.career || req.query.career || req.params.career;
        var studyPlan = req.body.studyPlan || req.query.studyPlan || req.params.studyPlan;
        var level = req.body.level || req.query.level || req.params.level;

        var aggregate = db.RegistrationToExaminationTables.aggregate();

        aggregate.lookup({
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user"
        });

        aggregate.unwind('$user');

        aggregate.lookup({
            "from": "student_files",
            "localField": "user._id",
            "foreignField": "user",
            "as": "file"
        });

        aggregate.unwind('$file');

        if(subject) {
            aggregate.match({'subjects': {'$eq': mongoose.Types.ObjectId(subject)}});
        }

        if(examinationTable) {
            aggregate.match({'examinationTable': {'$eq': mongoose.Types.ObjectId(examinationTable)}});
        }

        aggregate.group({
            "_id": "$user._id",
            "file":{ "$first": "$file" },
            "user":{ "$first": "$user" }
        });

        aggregate.sort({ 'file.personalData.lastName': 1 });

        aggregate.exec(function (error, resultData) {
            if (error) return res.status(404).send("Ha ocurrido un error.");

            db.Career.findById(career, function (err, career) {
                if (err) return res.status(404).send("Ha ocurrido un error.");

                var currentStudyPlan = career.studyPlans.filter(function (item) {
                    return item._id.equals(mongoose.Types.ObjectId(studyPlan));
                });
                if(currentStudyPlan && currentStudyPlan.length > 0) {
                    currentStudyPlan = currentStudyPlan[0];
                }

                var currentLevel = currentStudyPlan.levels.filter(function (item) {
                    return item._id.equals(mongoose.Types.ObjectId(level));
                });
                if(currentLevel && currentLevel.length > 0) {
                    currentLevel = currentLevel[0];
                }

                var currentSubject = currentLevel.subjects.filter(function (item) {
                    return item._id.equals(mongoose.Types.ObjectId(subject));
                });
                if(currentSubject && currentSubject.length > 0) {
                    currentSubject = currentSubject[0];
                }

                // Create a new instance of a Workbook class
                var wb = new xlsx.Workbook();

                // Add Worksheets to the workbook
                var ws = wb.addWorksheet('Inscriptos', { disableRowSpansOptimization: false });

                // Create a reusable style
                var headStyle = wb.createStyle({
                    font: {
                        color: '#ffffff',
                        size: 12,
                        bold: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '2172d7',
                    }
                });

                ws.cell(1,1).style(headStyle);
                ws.cell(1,2).style(headStyle);
                ws.cell(1,3).style(headStyle);
                ws.cell(1,4).style(headStyle);
                ws.cell(2,1).style(headStyle);
                ws.cell(2,4).style(headStyle);
                ws.cell(3,1).style(headStyle);
                ws.cell(3,4).style(headStyle);
                ws.cell(4,1).style(headStyle);
                ws.cell(4,4).style(headStyle);

                ws.cell(2, 2)
                    .string('Carrera:')
                    .style(headStyle);

                ws.cell(2, 3)
                    .string(career.name)
                    .style(headStyle);

                ws.cell(3, 2)
                    .string('Año:')
                    .style(headStyle);

                ws.cell(3, 3)
                    .string(currentLevel.name)
                    .style(headStyle);

                ws.cell(4, 2)
                    .string('Materia:')
                    .style(headStyle);

                ws.cell(4, 3)
                    .string(currentSubject.curricularSpace)
                    .style(headStyle);


                ws.cell(7, 1)
                    .string('#')
                    .style(headStyle);

                ws.cell(7, 2)
                    .string('DNI')
                    .style(headStyle);

                ws.cell(7, 3)
                    .string('Apellido y Nombre')
                    .style(headStyle);

                ws.cell(7, 4)
                    .string('Condición')
                    .style(headStyle);

                var row = 8;
                for (var i=0, total=resultData.length; i<total; i++) {

                    ws.cell(row, 1)
                        .number((i+1));

                    ws.cell(row, 2)
                        .number(resultData[i].file.personalData.dni);

                    ws.cell(row, 3)
                        .string(resultData[i].file.personalData.lastName + ' ' + resultData[i].file.personalData.firstName);

                    ws.cell(row, 4)
                        .string('');

                    row++;
                }

                if(resultData.length === 0) {
                    ws.cell(row, 1)
                        .string('No se han encontrado inscripciones a esta materia.');
                }

                wb.write('inscriptos_a_mesas.xlsx', res);
            });
        });
    }

};