var mongoose = require('mongoose');

module.exports = {

    list: function (req, res) {
        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;
        var career = req.body.career || req.query.career || req.params.career;
        var size = req.body.size || req.query.size || req.params.size;
        var status = req.body.status || req.query.status || req.params.status;

        var aggregate = db.StockOfAmbos.aggregate();
        aggregate.lookup({
            "from": "careers",
            "localField": "career",
            "foreignField": "_id",
            "as": "career"
        });
        aggregate.unwind('$career');

        if(career) {
            aggregate.match({'career._id': {'$eq': mongoose.Types.ObjectId(career)}});
        }

        if(size) {
            aggregate.match({'size': {'$eq': size}});
        }

        if(status && status === 'few-units') {
            aggregate.match({'amount': {'$lte': 10}});
        }
        if(status && status === 'without-stock') {
            aggregate.match({'amount': {'$eq': 0}});
        }

        var options = {
            sort: { 'createdAt' : -1 },
            page: Number(page),
            limit: Number(limit)
        };

        db.StockOfAmbos
            .aggregatePaginate(aggregate, options, function (error, items, pageCount, count) {
                if (error) return res.json({
                    sucess: false,
                    data: {
                        docs: [],
                        total: 0,
                        pages: 0,
                        page: 0
                    },
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: {
                        docs: items,
                        total: count,
                        page: page,
                        pages: pageCount
                    },
                    success: true,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var description = req.body.description || req.query.description || req.params.description;
        var career = req.body.career || req.query.career || req.params.career;
        var active = req.body.active || req.query.active || req.params.active || false;
        var price = req.body.price || req.query.price || req.params.price || 0;
        var amount = req.body.amount || req.query.amount || req.params.amount || 0;
        var size = req.body.size || req.query.size || req.params.size;

        var newItem = new db.StockOfAmbos({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            description: description,
            career: career,
            price: price,
            amount: amount,
            size: size,
            active: active
        });

        newItem.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: item_saved,
                message: 'El registro se ha creado exitosamente.'
            });
        });

    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var description = req.body.description || req.query.description || req.params.description;
        var career = req.body.career || req.query.career || req.params.career;
        var active = req.body.active || req.query.active || req.params.active || false;
        var price = req.body.price || req.query.price || req.params.price || 0;
        var amount = req.body.amount || req.query.amount || req.params.amount || 0;
        var size = req.body.size || req.query.size || req.params.size;

        db.StockOfAmbos.findById(id, function (err, item) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            item.name = name;
            item.active = active;
            item.lastModified = Date.now();
            item.description = description;
            item.career = career;
            item.price = price;
            item.amount = amount;
            item.size = size;

            item.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'El registro se ha guardado exitosamente.'
                });
            });

        });
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.StockOfAmbos.remove({'_id': id}, function (error, cmd) {
            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if (cmd.result.ok && cmd.result.n > 0) {
                return res.json({
                    success: true,
                    data: {
                        _id: id
                    },
                    message: 'La operación se ha realizado con éxito.'
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No se ha encontrado el registro que desea eliminar.'
                });
            }
        });

    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.StockOfAmbos.findById(id, function (err, item) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: item,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    },

    getNumberOfStudentsEnrolled :  function (req, res) {
        var currentTime = new Date();
        var currentYear = currentTime.getFullYear();

        db.RegistrationToCareer.aggregate([
            { "$match": { academicYear: currentYear,  $or: [
                { studentRegistrationStatus: 'enrolled-standard' },
                { studentRegistrationStatus: 'enrolled-for-exams' },
                { studentRegistrationStatus: 'enrolled-with-a-scholarship' }
            ]}},
            {
                "$lookup": {
                    "from": "student_files",
                    "localField": "user",
                    "foreignField": "user",
                    "as": "file"
                }
            },
            {
                "$group": {
                    "_id": { career: "$career", size: "$file.personalData.size" },
                    "career": { "$first": "$career" },
                    "size": { "$first" : "$file.personalData.size"},
                    "count": { "$sum": 1 }
                }
            }
        ]).exec(function (err, items) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: items,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};