var mongoose = require('mongoose');
var app = require('../../../../server');
//var mercadopago = require('mercadopago');
var mpGetCheckoutPreference = require('../../../middlewares/mp_checkout_preference');

module.exports = {

    /* Creamos las cuotas mensuales para todos los alumnos, esto debe ejecutarse con un cron todos los 1ros de cada mes a las 0 horas. */

    createMonthlyFee : function (req, res) {
        var currentTime = new Date();
        var currentYear = currentTime.getFullYear();
        var currentMonth = currentTime.getMonth() + 1;
        var initialMonth = 3;
        var finalMonth = 12;

        if(currentMonth >= initialMonth && currentMonth <= finalMonth) {
            db.RegistrationToCareer.find({
                'studentRegistrationStatus': 'enrolled-standard',
                'academicYear': currentYear
            }, function (err, enrolleds) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                enrolleds.forEach(function (inscription) {

                    db.Payment.findOne({'year': currentYear, 'month': currentMonth, 'user': inscription.user}, function (err, payment) {

                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        if(!payment) {

                            db.PaymentRulesForFee.findOne({career: inscription.career, active: true}, function (err, paymentRule) {

                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                if(paymentRule) {

                                    var dueDate = new Date();
                                    dueDate.setMonth(currentMonth-1);
                                    dueDate.setDate(paymentRule.expirationDay);
                                    dueDate.setFullYear(currentYear);
                                    dueDate.setHours(0);
                                    dueDate.setMinutes(0);
                                    dueDate.setMilliseconds(0);

                                    payment = new db.Payment({
                                        _id: new mongoose.Types.ObjectId(),
                                        user: inscription.user,
                                        title: 'Cuota ' + currentMonth + '/' + currentYear,
                                        paymentStatus: 'pending',
                                        paymentType: 'fee',
                                        amount: paymentRule.baseAmount, // importe a pagar
                                        amountPaid: 0, // importe pagado
                                        month: currentMonth,
                                        year: currentYear,
                                        dueDate: dueDate
                                    });

                                    payment.save();

                                }

                            });
                        }
                    });

                });

                return res.json({
                    success: true,
                    message: 'Las cuotas mensuales se han generado exitosamente.'
                });
            });
        } else {
            return res.json({
                success: false,
                message: 'Solo se pueden generar cuotas entre Marzo y Diciembre.'
            });
        }
    },

    getUserPayments : function (req, res) {

        var currentUser = req.decoded.data;

        if(currentUser) {

            db.Payment.find({'user': currentUser._id})
                .sort('-createdAt')
                .exec(function (err, payments) {

                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    return res.json({
                        data: payments,
                        success: true
                    });

                });

        }
    },

    getAllPayments : function (req, res) {

        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;
        var dni = req.body.dni || req.query.dni || req.params.dni;
        var email = req.body.email || req.query.email || req.params.email;
        var career = req.body.career || req.query.career || req.params.career;
        var paymentStatus = req.body.paymentStatus || req.query.paymentStatus || req.params.paymentStatus;
        var month = req.body.month || req.query.month || req.params.month;
        var year = req.body.year || req.query.year || req.params.year;

        var aggregate = db.Payment.aggregate();
        aggregate.lookup({
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user"
        });
        aggregate.unwind('$user');

        aggregate.lookup({
            "from": "student_files",
            "localField": "user._id",
            "foreignField": "user",
            "as": "file"
        });
        aggregate.unwind({
            "path": '$file',
            "preserveNullAndEmptyArrays": true
        });

        aggregate.lookup({
            "from": "careers",
            "localField": "file.currentCareer",
            "foreignField": "_id",
            "as": "career"
        });
        aggregate.unwind('$career');

        if (dni) {
            aggregate.match({'file.personalData.dni': {'$eq' : Number(dni)}})
        }
        if (email) {
            aggregate.match({'user.email': {'$eq' : email}})
        }
        if(career) {
            aggregate.match({'file.currentCareer': {'$eq' : mongoose.Types.ObjectId(career) }})
        }
        if(paymentStatus) {
            aggregate.match({'paymentStatus': {'$eq' : paymentStatus}})
        }
        if(month) {
            aggregate.match({'month': {'$eq' : Number(month)}})
        }
        if(year) {
            aggregate.match({'year': {'$eq' : Number(year)}})
        }

        var options = {
            sort: { createdAt: -1 },
            page: Number(page),
            limit: Number(limit)
        };

        aggregate.project({
            '_id': 1,
            'file.personalData.dni': 1,
            'file.personalData.lastName': 1,
            'file.personalData.firstName': 1,
            'file.currentCareer': 1,
            'file.currentLevel': 1,
            'career.name': 1,
            'career._id': 1,
            'title': 1,
            'description': 1,
            'paymentType': 1,
            'amount': 1,
            'amountPaid': 1,
            'paymentStatus': 1,
            'receiptNumber': 1,
            'createdAt': 1,
            'dueDate': 1
        })

        db.Payment
            .aggregatePaginate(aggregate, options, function (error, payments, pageCount, count) {
                if (error) return res.json({
                    sucess: false,
                    data: {
                        docs: [],
                        total: 0,
                        pages: 0,
                        page: 0
                    },
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: {
                        docs: payments,
                        total: count,
                        page: page,
                        pages: pageCount
                    },
                    success: true,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },

    payFee : function (req, res) {
        var paymentId = req.body._id || req.query._id || req.params._id;
        var currentUser = req.decoded.data;

        db.StudentFile.findOne({ user: currentUser._id })
            .exec(function (err, studentFile) {

                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if(studentFile) {

                    db.Payment.findById(paymentId)
                        .exec(function (err, payment) {

                            if (err) return res.json({
                                success: false,
                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                            });

                            if (payment) {

                                var description = '';

                                if (payment.paymentType == 'fee') {

                                    description = 'Pago de Cuota Mensual Instituto Superior Cruz Roja Argentina Filial Corrientes.';

                                    var args = {
                                        title: payment.title.replace('\/', '-') + " | " + String(studentFile.personalData.dni) + " | " + studentFile.personalData.lastName + " " + studentFile.personalData.firstName,
                                        description: description,
                                        price: payment.amount,
                                        picture_url: 'http://www.cruzroja.org.ar/corrientes/wp-content/uploads/2017/08/logo-cra.png',
                                        user: {
                                            email: currentUser.email,
                                            first_name: studentFile.personalData.firstName,
                                            last_name: studentFile.personalData.lastName,
                                            dni: String(studentFile.personalData.dni)
                                        },
                                        payment_id: paymentId
                                    };

                                    // Verificamos las reglas de pago y actualizamos los precios

                                    db.PaymentRulesForFee.findOne({
                                        career: studentFile.currentCareer,
                                        active: true
                                    }, function (err, paymentRule) {

                                        if (err) return res.json({
                                            success: false,
                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                        });

                                        if (paymentRule) {

                                            db.Career.findById(studentFile.currentCareer)
                                                .exec(function (err, career) {


                                                    if (err) return res.json({
                                                        success: false,
                                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                    });

                                                    if (career) {

                                                        var currentDate = new Date();
                                                        var interest = 0;
                                                        var diffDays = parseInt((currentDate - payment.dueDate) / (1000 * 60 * 60 * 24));


                                                        // si la cuota tiene configurado descuentos
                                                        if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                            args.price = args.price - paymentRule.discount.amount;
                                                        }


                                                        // si la cuota esta vencida sumamos los intereses
                                                        if (payment.dueDate.getTime() <= currentDate.getTime()) {

                                                            if (paymentRule.interests.length > 0) {

                                                                paymentRule.interests.forEach(function (i) {
                                                                    if (i.daysOfDelay <= diffDays) {
                                                                        interest = interest + i.amount;
                                                                    }
                                                                });

                                                                args.price = args.price + interest;
                                                            }
                                                        }

                                                        mpGetCheckoutPreference.regular_payment(args).then(function (mercadopago_preference) {

                                                            var mercadopago = app.get('mercadopago');

                                                            mercadopago.preferences.create(mercadopago_preference).then(function (data) {

                                                                var init_point = data.body.init_point;

                                                                if ('development' == app.get('env')) {
                                                                    init_point = data.body.sandbox_init_point;
                                                                }

                                                                var resultData = {
                                                                    preference: {
                                                                        payer: data.body.payer,
                                                                        init_point: init_point,
                                                                        items: data.body.items,
                                                                        id: data.body.id,
                                                                        external_reference: data.body.external_reference,
                                                                        auto_return: data.body.auto_return,
                                                                        back_urls: data.body.back_urls
                                                                    },
                                                                    payment: payment,
                                                                    interestAmount: interest,
                                                                    discountAmount: 0,
                                                                    amountPaid: args.price,
                                                                    career: career
                                                                };

                                                                if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                                    resultData.discountAmount = paymentRule.discount.amount;
                                                                }

                                                                return res.json({
                                                                    data: resultData,
                                                                    success: true
                                                                });

                                                            }).catch(function (error) {
                                                                return res.json({
                                                                    success: false,
                                                                    message: error.message
                                                                });
                                                            });

                                                        });

                                                    } else {

                                                        return res.json({
                                                            success: false,
                                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                        });

                                                    }
                                                });

                                        } else {

                                            return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                            });

                                        }
                                    });

                                } else if (payment.paymentType == 'enrollment') {

                                    description = 'Pago de Matrícula Instituto Superior Cruz Roja Argentina Filial Corrientes.';

                                    var args = {
                                        title: payment.title.replace('\/', '-') + " | " + String(studentFile.personalData.dni) + " | " + studentFile.personalData.lastName + " " + studentFile.personalData.firstName,
                                        description: description,
                                        price: payment.amount,
                                        picture_url: 'http://www.cruzroja.org.ar/corrientes/wp-content/uploads/2017/08/logo-cra.png',
                                        user: {
                                            email: currentUser.email,
                                            first_name: studentFile.personalData.firstName,
                                            last_name: studentFile.personalData.lastName,
                                            dni: String(studentFile.personalData.dni)
                                        },
                                        payment_id: paymentId
                                    };

                                    // Verificamos las reglas de pago y actualizamos los precios

                                    db.PaymentRulesForTuition.findOne({
                                        career: studentFile.currentCareer,
                                        active: true
                                    }, function (err, paymentRule) {

                                        if (err) return res.json({
                                            success: false,
                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                        });

                                        if (paymentRule) {

                                            db.Career.findById(studentFile.currentCareer)
                                                .exec(function (err, career) {


                                                    if (err) return res.json({
                                                        success: false,
                                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                    });

                                                    if (career) {

                                                        var currentDate = new Date();
                                                        var interest = 0;
                                                        var diffDays = parseInt((currentDate - payment.dueDate) / (1000 * 60 * 60 * 24));


                                                        // si la cuota tiene configurado descuentos
                                                        if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                            args.price = args.price - paymentRule.discount.amount;
                                                        }


                                                        // si la cuota esta vencida sumamos los intereses
                                                        if (payment.dueDate.getTime() <= currentDate.getTime()) {

                                                            if (paymentRule.interests.length > 0) {

                                                                paymentRule.interests.forEach(function (i) {
                                                                    if (i.daysOfDelay <= diffDays) {
                                                                        interest = interest + i.amount;
                                                                    }
                                                                });

                                                                args.price = args.price + interest;
                                                            }
                                                        }

                                                        mpGetCheckoutPreference.regular_payment(args).then(function (mercadopago_preference) {

                                                            var mercadopago = app.get('mercadopago');

                                                            mercadopago.preferences.create(mercadopago_preference).then(function (data) {

                                                                var init_point = data.body.init_point;

                                                                if ('development' == app.get('env')) {
                                                                    init_point = data.body.sandbox_init_point;
                                                                }

                                                                var resultData = {
                                                                    preference: {
                                                                        payer: data.body.payer,
                                                                        init_point: init_point,
                                                                        items: data.body.items,
                                                                        id: data.body.id,
                                                                        external_reference: data.body.external_reference,
                                                                        auto_return: data.body.auto_return,
                                                                        back_urls: data.body.back_urls
                                                                    },
                                                                    payment: payment,
                                                                    interestAmount: interest,
                                                                    discountAmount: 0,
                                                                    amountPaid: args.price,
                                                                    career: career
                                                                };

                                                                if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                                    resultData.discountAmount = paymentRule.discount.amount;
                                                                }

                                                                return res.json({
                                                                    data: resultData,
                                                                    success: true
                                                                });

                                                            }).catch(function (error) {
                                                                return res.json({
                                                                    success: false,
                                                                    message: error.message
                                                                });
                                                            });

                                                        });

                                                    } else {

                                                        return res.json({
                                                            success: false,
                                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                        });

                                                    }
                                                });

                                        } else {

                                            return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                            });

                                        }
                                    });

                                } else if (payment.paymentType == 'custom') {

                                    description = 'Pago de Instituto Superior Cruz Roja Argentina Filial Corrientes.';

                                    var args = {
                                        title: payment.title.replace('\/', '-') + " | " + String(studentFile.personalData.dni) + " | " + studentFile.personalData.lastName + " " + studentFile.personalData.firstName,
                                        description: description,
                                        price: payment.amount,
                                        picture_url: 'http://www.cruzroja.org.ar/corrientes/wp-content/uploads/2017/08/logo-cra.png',
                                        user: {
                                            email: currentUser.email,
                                            first_name: studentFile.personalData.firstName,
                                            last_name: studentFile.personalData.lastName,
                                            dni: String(studentFile.personalData.dni)
                                        },
                                        payment_id: paymentId
                                    };

                                    db.Career.findById(studentFile.currentCareer)
                                        .exec(function (err, career) {

                                            if (err) return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                            });

                                            if (career) {

                                                mpGetCheckoutPreference.regular_payment(args).then(function (mercadopago_preference) {

                                                    var mercadopago = app.get('mercadopago');

                                                    mercadopago.preferences.create(mercadopago_preference).then(function (data) {

                                                        var init_point = data.body.init_point;

                                                        if ('development' == app.get('env')) {
                                                            init_point = data.body.sandbox_init_point;
                                                        }

                                                        var resultData = {
                                                            preference: {
                                                                payer: data.body.payer,
                                                                init_point: init_point,
                                                                items: data.body.items,
                                                                id: data.body.id,
                                                                external_reference: data.body.external_reference,
                                                                auto_return: data.body.auto_return,
                                                                back_urls: data.body.back_urls
                                                            },
                                                            payment: payment,
                                                            interestAmount: 0,
                                                            discountAmount: 0,
                                                            amountPaid: args.price,
                                                            career: career
                                                        };


                                                        return res.json({
                                                            data: resultData,
                                                            success: true
                                                        });

                                                    }).catch(function (error) {
                                                        return res.json({
                                                            success: false,
                                                            message: error.message
                                                        });
                                                    });

                                                });


                                            } else {

                                                return res.json({
                                                    success: false,
                                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                });

                                            }
                                        });


                                } else {

                                    return res.json({
                                        success: false,
                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                    });

                                }

                            }

                        });

                    } else {

                        return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });
                    }

            });

    },

    get : function (req, res) {
        var paymentId = req.body._id || req.query._id || req.params._id;

        db.Payment.findById(paymentId)
            .exec(function (err, payment) {

                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if (payment) {

                    db.StudentFile.findOne({ user: payment.user })
                        .exec(function (err, studentFile) {

                            if (err) return res.json({
                                success: false,
                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                            });

                            if (studentFile) {

                                var args = {
                                    title: payment.title.replace('\/', '-'),
                                    price: payment.amount,
                                    payment_id: paymentId
                                };

                                if( payment.paymentType == 'fee' ) {

                                    // Verificamos las reglas de pago para las cuotas y actualizamos los precios

                                    db.PaymentRulesForFee.findOne({
                                        career: studentFile.currentCareer,
                                        active: true
                                    }, function (err, paymentRule) {

                                        if (err) return res.json({
                                            success: false,
                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                        });

                                        if (paymentRule) {

                                            db.Career.findById(studentFile.currentCareer)
                                                .exec(function (err, career) {


                                                    if (err) return res.json({
                                                        success: false,
                                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                    });

                                                    if (career) {

                                                        var currentDate = new Date();
                                                        var interest = 0;
                                                        var diffDays = parseInt((currentDate - payment.dueDate) / (1000 * 60 * 60 * 24));


                                                        // si la cuota tiene configurado descuentos
                                                        if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                            args.price = args.price - paymentRule.discount.amount;
                                                        }


                                                        // si la cuota esta vencida sumamos los intereses
                                                        if (payment.dueDate.getTime() <= currentDate.getTime()) {

                                                            if (paymentRule.interests.length > 0) {

                                                                paymentRule.interests.forEach(function (i) {
                                                                    if (i.daysOfDelay <= diffDays) {
                                                                        interest = interest + i.amount;
                                                                    }
                                                                });

                                                                args.price = args.price + interest;
                                                            }
                                                        }

                                                        var resultData = {
                                                            payment: payment,
                                                            interestAmount: interest,
                                                            discountAmount: 0,
                                                            totalAmount: args.price,
                                                            career: career,
                                                            studentFile: studentFile
                                                        };

                                                        if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                            resultData.discountAmount = paymentRule.discount.amount;
                                                        }

                                                        return res.json({
                                                            data: resultData,
                                                            success: true
                                                        });

                                                    } else {

                                                        return res.json({
                                                            success: false,
                                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                        });

                                                    }
                                                });


                                        } else {

                                            return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                            });

                                        }
                                    });

                                } else if ( payment.paymentType == 'enrollment' ) {

                                    // Verificamos las reglas de pago para la matricula y actualizamos los precios

                                    db.PaymentRulesForTuition.findOne({
                                        career: studentFile.currentCareer,
                                        active: true
                                    }, function (err, paymentRule) {

                                        if (err) return res.json({
                                            success: false,
                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                        });

                                        if (paymentRule) {

                                            db.Career.findById(studentFile.currentCareer)
                                                .exec(function (err, career) {


                                                    if (err) return res.json({
                                                        success: false,
                                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                    });

                                                    if (career) {

                                                        var currentDate = new Date();
                                                        var interest = 0;
                                                        var diffDays = parseInt((currentDate - payment.dueDate) / (1000 * 60 * 60 * 24));


                                                        // si la cuota tiene configurado descuentos
                                                        if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                            args.price = args.price - paymentRule.discount.amount;
                                                        }


                                                        // si la cuota esta vencida sumamos los intereses
                                                        if (payment.dueDate.getTime() <= currentDate.getTime()) {

                                                            if (paymentRule.interests.length > 0) {

                                                                paymentRule.interests.forEach(function (i) {
                                                                    if (i.daysOfDelay <= diffDays) {
                                                                        interest = interest + i.amount;
                                                                    }
                                                                });

                                                                args.price = args.price + interest;
                                                            }
                                                        }

                                                        var resultData = {
                                                            payment: payment,
                                                            interestAmount: interest,
                                                            discountAmount: 0,
                                                            totalAmount: args.price,
                                                            career: career,
                                                            studentFile: studentFile
                                                        };

                                                        if (paymentRule.discount.untilDay >= currentDate.getDate() && diffDays <= 0) {
                                                            resultData.discountAmount = paymentRule.discount.amount;
                                                        }

                                                        return res.json({
                                                            data: resultData,
                                                            success: true
                                                        });

                                                    } else {

                                                        return res.json({
                                                            success: false,
                                                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                        });

                                                    }
                                                });


                                        } else {

                                            return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error, no hay reglas de pago. Intentelo nuevamente mas tarde.'
                                            });

                                        }
                                    });

                                } else if ( payment.paymentType == 'custom' ) {

                                    db.Career.findById(studentFile.currentCareer)
                                        .exec(function (err, career) {


                                            if (err) return res.json({
                                                success: false,
                                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                            });

                                            if (career) {

                                                var resultData = {
                                                    payment: payment,
                                                    interestAmount: 0,
                                                    discountAmount: 0,
                                                    totalAmount: payment.amount,
                                                    career: career,
                                                    studentFile: studentFile
                                                };

                                                return res.json({
                                                    data: resultData,
                                                    success: true
                                                });

                                            } else {

                                                return res.json({
                                                    success: false,
                                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                });

                                            }
                                        });


                                }

                            } else {

                                return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                            }

                        });

                } else {

                    return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });
                }

            });
    },

    processPayment : function (req, res) {

        var mpPaymentId = req.body.id || req.query.id || req.params.id;
        var mpTopic = req.body.topic || req.query.topic || req.params.topic;

        var mercadopago = app.get('mercadopago');

        switch (mpTopic) {

            case "payment":
                mercadopago.getPayment (mpPaymentId, function (err, data){
                    if (err) {

                        return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                    } else {

                        var paymentId = data.external_reference;

                        db.Payment.findById(paymentId, function (err, payment) {

                            if (err) {
                                return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });
                            }

                            payment.paymentStatus = data.status;
                            payment.mercadopagoPaymentId = mpPaymentId;
                            payment.amountPaid = data.transaction_amount;
                            payment.paymentDate = new Date();
                            payment.lastModified = new Date();
                            payment.paymentMethod = 'mercadopago';

                            payment.save();

                            return res.status(200).send({
                                data: data,
                                success: true
                            });
                        });

                    }
                });
                break;


            case "authorized_payment":
                mercadopago.getAuthorizedPayment (mpPaymentId, function (err, data){
                    if (err) {
                        return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });
                    } else {
                        return res.status(200).send({
                            data: data,
                            success: true
                        });
                    }
                });
                break;


            case "preapproval":
                mercadopago.getPreapprovalPayment (mpPaymentId, function (err, data){
                    if (err) {
                        return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });
                    } else {
                        return res.status(200).send({
                            data: data,
                            success: true
                        });
                    }
                });
                break;

        }
    },



    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var paymentStatus = req.body.paymentStatus || req.query.paymentStatus || req.params.paymentStatus;
        var amountPaid = req.body.amountPaid || req.query.amountPaid || req.params.amountPaid || 0;
        var paymentMethod = req.body.paymentMethod || req.query.paymentMethod || req.params.paymentMethod;
        var receiptNumber = req.body.receiptNumber || req.query.receiptNumber || req.params.receiptNumber;
        var amount =  req.body.amount || req.query.amount || req.params.amount || 0;

        db.Payment.findById(id, function (err, payment) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            payment.paymentStatus = paymentStatus;
            payment.amountPaid = amountPaid;
            payment.paymentDate = new Date();
            payment.lastModified = new Date();
            payment.paymentMethod = paymentMethod;
            payment.receiptNumber = receiptNumber;
            payment.amount = amount;

            payment.save(function (err, item_saved) {

                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if( item_saved.paymentType == 'enrollment' && item_saved.paymentStatus == 'approved') {

                    db.RegistrationToCareer.findOne({user: item_saved.user, academicYear: item_saved.year, $or: [{'studentRegistrationStatus': 'final-approved' }, {'studentRegistrationStatus': 'conditional-approved' }] }, function (err, item) {
                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });
                        // Si el alumno esta inscripto al cliclo lectivo y la matriula se pago, marcamos la inscripcion a la carrera como matriculado
                        if(item) {
                            item.studentRegistrationStatus = 'enrolled-standard';
                            item.save();
                        }
                    });

                }

                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'La regla se ha guardado exitosamente.'
                });
            });
        })
    },


    create : function (req, res) {
        var paymentStatus = req.body.paymentStatus || req.query.paymentStatus || req.params.paymentStatus;
        var title = req.body.title || req.query.title || req.params.title || '';
        var paymentMethod = req.body.paymentMethod || req.query.paymentMethod || req.params.paymentMethod;
        var paymentType = req.body.paymentType || req.query.paymentType || req.params.paymentType;
        var amount =  req.body.amount || req.query.amount || req.params.amount || 0;
        var description = req.body.description || req.query.description || req.params.description;
        var user = req.body.user || req.query.user || req.params.user;
        var month = req.body.month || req.query.month || req.params.month;
        var year = req.body.year || req.query.year || req.params.year;
        var dueDate = req.body.dueDate || req.query.dueDate || req.params.dueDate;
        var receiptNumber = req.body.receiptNumber || req.query.receiptNumber || req.params.receiptNumber;
        var paymentDate = req.body.paymentDate || req.query.paymentDate || req.params.paymentDate;
        var amountPaid = req.body.amountPaid || req.query.amountPaid || req.params.amountPaid || 0;

        var userId = user.value;

        var payment = new db.Payment({
            _id: new mongoose.Types.ObjectId(),
            paymentStatus: paymentStatus,
            amount: amount,
            paymentMethod: paymentMethod,
            title: title,
            paymentType: paymentType,
            description: description,
            user: userId,
            year: year,
            month: month,
            dueDate: dueDate,
            receiptNumber: receiptNumber,
            paymentDate: paymentDate,
            amountPaid: amountPaid
        });

        payment.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'El pago se ha creado exitosamente.'
            });
        });
    },


    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.Payment.remove({'_id': id}, function (error, cmd) {
            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if (cmd.result.ok && cmd.result.n > 0) {
                return res.json({
                    success: true,
                    data: {
                        _id: id
                    },
                    message: 'La operación se ha realizado con éxito.'
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No se ha encontrado el registro que desea eliminar.'
                });
            }
        });
    },


    upgradeBaseAmount: function (req, res) {

        var baseAmount = req.body.baseAmount || req.query.baseAmount || req.params.baseAmount || 0;
        var career = req.body.career || req.query.career || req.params.career;

        if(Number(baseAmount) > 0) {

            db.Payment.find({
                'paymentStatus': 'pending',
                'paymentType': 'fee',
                'amount': { $ne: baseAmount }
            }, function (err, payments) {

                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                payments.forEach(function(payment) {

                    db.RegistrationToCareer.findOne({ user: payment.user, academicYear: payment.year, career: career }, function (err, inscription) {
                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        if (inscription) {

                            payment.amount = baseAmount;
                            payment.save();

                        } else {
                            // En caso de que se haya eliminado la inscripcion a la carrera, buscamos en el legado la ultima carrera cursada
                            db.StudentFile.findOne({ user: payment.user, currentCareer: career }, function (err, studentFile) {
                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                if(studentFile) {

                                    payment.amount = baseAmount;
                                    payment.save();

                                }
                            });
                        }
                    });

                });

                return res.json({
                    success: true,
                    data: payments,
                    message: 'La operación ha sido exitosa. Pueden demorar unos minutos en impactar los montos nuevos.'
                });

            });

        } else {

            return res.json({
                success: false,
                message: 'Ha ocurrido un error. El monto base debe ser mayor a 0.'
            });

        }
    }
};