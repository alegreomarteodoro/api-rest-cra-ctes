var sendMail = require('../../../middlewares/send_mail');
var crypto   = require('crypto');
var mongoose = require('mongoose');

module.exports = {

    list: function (req, res) {
        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;
        var dni = req.body.dni || req.query.dni || req.params.dni;
        var email = req.body.email || req.query.email || req.params.email;
        var role = req.body.role || req.query.role || req.params.role;

        var aggregate = db.User.aggregate();
        aggregate.lookup({
            "from": "roles",
            "localField": "role",
            "foreignField": "_id",
            "as": "role"
        });
        aggregate.unwind('$role');

        aggregate.lookup({
            "from": "student_files",
            "localField": "_id",
            "foreignField": "user",
            "as": "file"
        });
        aggregate.unwind({
            "path": '$file',
            "preserveNullAndEmptyArrays": true
        });

        if (dni) {
            aggregate.match({'file.personalData.dni': {'$eq' : Number(dni)}})
        }
        if (email) {
            aggregate.match({email: {'$eq' : email}})
        }
        if (role) {
            aggregate.match({'role._id': {'$eq' : mongoose.Types.ObjectId(role)}})
        }

        var options = {
            sort: { 'createdAt' : -1 },
            page: Number(page),
            limit: Number(limit)
        };

        db.User
            .aggregatePaginate(aggregate, options, function (error, users, pageCount, count) {
                if (error) return res.json({
                    sucess: false,
                    data: {
                        docs: [],
                        total: 0,
                        pages: 0,
                        page: 0
                    },
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: {
                        docs: users,
                        total: count,
                        page: page,
                        pages: pageCount
                    },
                    success: true,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },
    
    register: function (req, res) {
        var password = req.body.password || req.query.password || req.params.password;
        var email = req.body.email || req.query.email || req.params.email;
        var active = req.body.active || req.query.active || req.params.active || false;
        var admin = req.body.admin || req.query.admin || req.params.admin || false;
        var role = req.body.role || req.query.role || req.params.role;
        var dni = req.body.dni || req.query.dni || req.params.dni;
        var displayName = req.body.displayName || req.query.displayName || req.params.displayName;

        var user = {
            email: email,
            active: active,
            admin: admin,
            displayName: displayName,
            role: null
        };

        if(role && !admin) {

            user.role = role;

        }

        if (!role) {

            db.Role.findOne({isDefault: true}, function (err, defaultRole) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if (defaultRole) {



                    if (dni) {

                        db.StudentFile.findOne({'personalData.dni': dni})
                            .populate('user')
                            .exec(function (err, studentFile) {
                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                if (studentFile) {

                                    return res.json({
                                        success: false,
                                        data: null,
                                        message: 'No te puedes volver a registrar. Ya estas registrado con el correo ' + studentFile.user.email + '. Inicia sesión con esa cuenta de correo.'
                                    });

                                } else {

                                    user.role = defaultRole;

                                    db.User.register(user, password, function (err, user) {
                                        if (err) {
                                            if (err.name === 'MissingPasswordError') {
                                                return res.json({
                                                    success: false,
                                                    message: 'Ingrese la contraseña.'
                                                });
                                            } else if (err.name === 'UserExistsError') {
                                                return res.json({
                                                    success: false,
                                                    message: 'El email que intenta registrar ya esta en uso.'
                                                });
                                            } else {
                                                return res.json({
                                                    success: false,
                                                    message: 'Ha ocurrido un error cuando se registraba el usuario.'
                                                });
                                            }

                                        } else {
                                            user.populate('role');

                                            var codeLength = 15;

                                            var ramdomCode = crypto.randomBytes(Math.ceil(codeLength * 3 / 4))
                                                .toString('base64')   // convert to base64 format
                                                .slice(0, codeLength)        // return required number of characters
                                                .replace(/\+/g, '0')  // replace '+' with '0'
                                                .replace(/\//g, '0'); // replace '/' with '0'

                                            var activationLink = 'http://www.cruzrojacorrientes.org/#/verify-account/' + ramdomCode;

                                            var emailHtmlContent = '<p><b>Bienvenido a Cruz Roja.</b></p>' +
                                                '<p>Te has registrado con el email: <b>' + user.email + '</b> y tu contraseña es: <b>' + password + '</b></p><br>' +
                                                '<p>Para activar tu cuenta has click en el link: <a href="' + activationLink + '">' + activationLink + '</a>.</p>' +
                                                '<p><small>Si cuando ingresas al link no se completa el código automáticamente, ingresalo manualmente, y has click en Activar.</small></p>' +
                                                '<i>El código de activación es:</i> <b>' + ramdomCode + '</b>';

                                            user.activationCode = ramdomCode;

                                            user.save(function (err, item_saved) {
                                                if (err) return res.json({
                                                    success: false,
                                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                                });

                                                sendMail(user.email, 'Bienvenido a Cruz Roja', emailHtmlContent);

                                                return res.json({
                                                    data: user,
                                                    success: true,
                                                    message: 'El usuario se ha registrado exitosamente.'
                                                });
                                            });


                                        }
                                    });

                                }
                            });


                    } else {

                        user.role = defaultRole;

                        db.User.register(user, password, function (err, user) {
                            if (err) {
                                if (err.name === 'MissingPasswordError') {
                                    return res.json({
                                        success: false,
                                        message: 'Ingrese la contraseña.'
                                    });
                                } else if (err.name === 'UserExistsError') {
                                    return res.json({
                                        success: false,
                                        message: 'El email que intenta registrar ya esta en uso.'
                                    });
                                } else {
                                    return res.json({
                                        success: false,
                                        message: 'Ha ocurrido un error cuando se registraba el usuario.'
                                    });
                                }

                            } else {
                                user.populate('role');

                                var codeLength = 15;

                                var ramdomCode = crypto.randomBytes(Math.ceil(codeLength * 3 / 4))
                                    .toString('base64')   // convert to base64 format
                                    .slice(0, codeLength)        // return required number of characters
                                    .replace(/\+/g, '0')  // replace '+' with '0'
                                    .replace(/\//g, '0'); // replace '/' with '0'

                                var activationLink = 'http://www.cruzrojacorrientes.org/#/verify-account/' + ramdomCode;

                                var emailHtmlContent = '<p><b>Bienvenido a Cruz Roja.</b></p>' +
                                    '<p>Te has registrado con el email: <b>' + user.email + '</b> y tu contraseña es: <b>' + password + '</b></p><br>' +
                                    '<p>Para activar tu cuenta has click en el link: <a href="' + activationLink + '">' + activationLink + '</a>.</p>' +
                                    '<p><small>Si cuando ingresas al link no se completa el código automáticamente, ingresalo manualmente, y has click en Activar.</small></p>' +
                                    '<i>El código de activación es:</i> <b>' + ramdomCode + '</b>';

                                user.activationCode = ramdomCode;

                                user.save(function (err, item_saved) {
                                    if (err) return res.json({
                                        success: false,
                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                    });

                                    sendMail(user.email, 'Bienvenido a Cruz Roja', emailHtmlContent);

                                    return res.json({
                                        data: user,
                                        success: true,
                                        message: 'El usuario se ha registrado exitosamente.'
                                    });
                                });


                            }
                        });


                    }

                }
            });

        } else {

            if (dni) {

                db.StudentFile.findOne({'personalData.dni': dni})
                    .populate('user')
                    .exec(function (err, studentFile) {
                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        if(studentFile) {

                            return res.json({
                                success: false,
                                data: null,
                                message: 'No te puedes volver a registrar. Ya estas registrado con el correo ' + studentFile.user.email + '. Inicia sesión con esa cuenta de correo.'
                            });

                        } else {

                            db.User.register(user, password, function (err, user) {
                                if (err) {
                                    if (err.name === 'MissingPasswordError') {
                                        return res.json({
                                            success: false,
                                            message: 'Ingrese la contraseña.'
                                        });
                                    } else if (err.name === 'UserExistsError') {
                                        return res.json({
                                            success: false,
                                            message: 'El email que intenta registrar ya esta en uso.'
                                        });
                                    } else {
                                        return res.json({
                                            success: false,
                                            message: 'Ha ocurrido un error cuando se registraba el usuario.'
                                        });
                                    }

                                } else {
                                    user.populate('role');

                                    return res.json({
                                        data: user,
                                        success: true,
                                        message: 'El usuario se ha registrado exitosamente.'
                                    });
                                }
                            });

                        }
                    });

            } else {

                db.User.register(user, password, function (err, user) {
                    if (err) {
                        if (err.name === 'MissingPasswordError') {
                            return res.json({
                                success: false,
                                message: 'Ingrese la contraseña.'
                            });
                        } else if (err.name === 'UserExistsError') {
                            return res.json({
                                success: false,
                                message: 'El email que intenta registrar ya esta en uso.'
                            });
                        } else {
                            return res.json({
                                success: false,
                                message: 'Ha ocurrido un error cuando se registraba el usuario.'
                            });
                        }

                    } else {
                        user.populate('role');

                        return res.json({
                            data: user,
                            success: true,
                            message: 'El usuario se ha registrado exitosamente.'
                        });
                    }
                });
            }
        }
    },

    change_password: function (req, res) {
        var email = req.body.email || req.query.email || req.params.email;
        var password = req.body.password || req.query.password || req.params.password;

        db.User.findByUsername(email).then(function(sanitizedUser){
            if (sanitizedUser){
                sanitizedUser.setPassword(password, function(){
                    sanitizedUser.save();
                    res.json({
                        success: true,
                        data: sanitizedUser,
                        message: 'La contraseña ha sido cambiada.'
                    });
                });
            } else {
                res.json({
                    sucess: false,
                    message: 'El usuario no existe.'
                });
            }
        },function(err){
            console.error({
                sucess: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
        })
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.StudentFile.findOne({ user: id }, function(err, studentFile) {

            if (err) return res.json({
                success: false,
                data: err,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if(studentFile) {

                return res.json({
                    success: false,
                    message: 'No se puede eliminar el registro porque tiene datos asociados a otros modulos.'
                });

            } else {

                db.User.remove({'_id': id}, function (error, cmd) {
                    if (error) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if(cmd.result.ok && cmd.result.n > 0) {
                        return res.json({
                            success: true,
                            data: {
                                _id: id
                            },
                            message: 'La operación se ha realizado con éxito.'
                        });
                    } else {
                        return res.json({
                            success: false,
                            message: 'No se ha encontrado el registro que desea eliminar.'
                        });
                    }
                });

            }


        });
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var displayName = req.body.displayName || req.query.displayName || req.params.displayName;
        var password = req.body.password || req.query.password || req.params.password;
        var email = req.body.email || req.query.email || req.params.email;
        var active = req.body.active || req.query.active || req.params.active || false;
        var admin = req.body.admin || req.query.admin || req.params.admin || false;
        var role = req.body.role || req.query.role || req.params.role;

        db.User.findById(id, function (err, user) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            user.displayName = displayName;
            user.active = active;
            user.lastModified = Date.now();
            user.email = email;
            user.admin = admin;

            if(!admin) {
                user.role = role;
            } else {
                user.role = null;
            }

            if(password) {
                user.setPassword(password, function (err, user) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    user.save();
                });
            }

            user.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'El rol se ha creado exitosamente.'
                });
            });
        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.User.findById(id)
            .exec(function (err, user) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    success: true,
                    data: user,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },


    veriryAccount : function (req, res) {
        var activationCode = req.body.activationCode || req.query.activationCode || req.params.activationCode;

        db.User.findOne({ 'activationCode': activationCode })
            .exec(function (err, user) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if(user) {

                    user.verifiedAccount = true;
                    user.active = true;
                    user.activationCode = '';

                    user.save(function (err, item_saved) {
                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        return res.json({
                            success: true,
                            data: item_saved,
                            message: 'Tu cuenta ha sido activada.'
                        });
                    });

                } else {

                    return res.json({
                        success: false,
                        message: 'El código de activación que envió ya ha expirado.'
                    });

                }
            });
    }

};