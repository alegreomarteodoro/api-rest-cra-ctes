var jwt = require('jsonwebtoken');
var app = require('../../../../server');

module.exports = {

    authenticate : function(req, res) {

        var email = req.body.email || req.query.email || req.param.email;
        var password = req.body.password || req.query.password || req.param.password;

        var authenticate = db.User.authenticate();

        authenticate(email, password, function(err, user) {
            if (err) throw err;
            if (!user) {
                res.json({
                    success: false,
                    message: 'Email o contraseña inválidos.',
                    user: {
                        email: user.email
                    }
                });
            } else if (user && user.active) {

                // create a token
                var token = jwt.sign({data: user}, app.get('superSecret'), {
                    expiresIn: 604800 // 1 week
                });

                if(user.role) {
                    user.populate('role', function (err, user) {
                        if(user.role.permissions) {
                            user.role.populate('permissions', function (err, role) {

                                res.json({
                                    success: true,
                                    message: 'El token ha sido creado.',
                                    access_token: token,
                                    user: {
                                        email: user.email,
                                        admin: user.admin,
                                        role: role,
                                        displayName: user.displayName,
                                        _id: user._id
                                    }
                                });

                            });
                        } else {
                            res.json({
                                success: true,
                                message: 'El token ha sido creado.',
                                access_token: token,
                                user: {
                                    email: user.email,
                                    admin: user.admin,
                                    role: role,
                                    displayName: user.displayName,
                                    _id: user._id
                                }
                            });
                        }
                    });
                } else {
                    res.json({
                        success: true,
                        message: 'El token ha sido creado.',
                        access_token: token,
                        user: {
                            email: user.email,
                            admin: user.admin,
                            displayName: user.displayName,
                            _id: user._id
                        }
                    });
                }
            } else {
                res.json({
                    success: false,
                    message: 'No se ha encontrado el usuario o está inactivo.',
                    user: {
                        email: user.email
                    }
                });
            }
        });
    }

};