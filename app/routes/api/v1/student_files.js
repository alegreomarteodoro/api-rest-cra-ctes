var mongoose = require('mongoose');
module.exports = {

    list : function (req, res) {

        var aggregate = db.StudentFile.aggregate();

        aggregate.lookup({
            "from": "registration_to_careers",
            "localField": "user",
            "foreignField": "user",
            "as": "registration_to_career"
        });
        aggregate.unwind('$registration_to_career');

        aggregate.lookup({
            "from": "careers",
            "localField": "registration_to_career.career",
            "foreignField": "_id",
            "as": "current_career"
        });
        aggregate.unwind('$current_career');

        aggregate.match({ $or: [
            { 'registration_to_career.studentRegistrationStatus': 'enrolled-standard' },
            { 'registration_to_career.studentRegistrationStatus': 'enrolled-for-exams' },
            { 'registration_to_career.studentRegistrationStatus': 'enrolled-with-a-scholarship' }
        ]});

        aggregate.group({
            '_id' : "$_id",
            "personalData":{ "$first": "$personalData" },
            "user":{ "$first": "$user" },
            "registration_to_career":{ "$first": "$registration_to_career" },
            "current_career":{ "$first": "$current_career.name" }
        });

        aggregate.sort({'personalData.lastName': 1, 'personalData.firstName': 1});

        aggregate.exec(function (error, studentsFile) {
                if (error) return res.json({
                    success: false,
                    data: error,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    data: studentsFile,
                    success: true
                });
            });
    },

    getByUser : function (req, res) {
        var user = req.body.user || req.query.user || req.params.user;

        db.StudentFile.findOne({ user: user })
            .exec(function (err, studentFile) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                return res.json({
                    success: true,
                    data: studentFile,
                    message: 'La operación se ha realizado con éxito.'
                });
            });
    },

    create : function (req, res) {
        var studentFile = req.body.studentFile || req.query.studentFile || req.param.studentFile;
        var career = req.body.career || req.query.career || req.param.career;
        var level = req.body.level || req.query.level || req.param.level;


        var dbStudentFile = new db.StudentFile({
            _id: new mongoose.Types.ObjectId(),
            personalData: studentFile.personalData,
            career: career,
            level: level,
            treasury: studentFile.treasury
        });

        dbStudentFile.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                data: err,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'El legajo se ha creado exitosamente.'
            });
        });
        
    },

    edit : function (req, res) {
        var studentFile = req.body.studentFile || req.query.studentFile || req.param.studentFile;

        if (studentFile.user) {

            db.StudentFile.findOne({ user: studentFile.user }, function(err, dbStudentFile) {

                if (err) return res.json({
                    success: false,
                    data: err,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if(dbStudentFile) {
                    dbStudentFile.user = studentFile.user;
                    dbStudentFile.currentLevel = studentFile.currentLevel;
                    dbStudentFile.currentCareer = studentFile.currentCareer;
                    dbStudentFile.personalData = studentFile.personalData;
                    dbStudentFile.treasury = studentFile.treasury;
                } else {
                    dbStudentFile = new db.StudentFile({
                        _id: new mongoose.Types.ObjectId(),
                        user: studentFile.user,
                        currentLevel: studentFile.currentLevel,
                        currentCareer: studentFile.currentCareer,
                        personalData: studentFile.personalData,
                        treasury:studentFile.treasury
                    });
                }

                dbStudentFile.save(function (err, item_saved) {
                    if (err) return res.json({
                        success: false,
                        data: err,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    return res.json({
                        success: true,
                        data: item_saved,
                        message: 'El legajo se ha modificado exitosamente.'
                    });
                });

            });

        } else {

            return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

        }
    },

    listStudentsOfCurrentAcademicYear : function (req, res) {

        var currentDate = new Date();

        var aggregate = db.StudentFile.aggregate();

        aggregate.lookup({
            "from": "registration_to_careers",
            "localField": "user",
            "foreignField": "user",
            "as": "registration_to_career"
        });
        aggregate.unwind('$registration_to_career');

        aggregate.lookup({
            "from": "careers",
            "localField": "registration_to_career.career",
            "foreignField": "_id",
            "as": "current_career"
        });
        aggregate.unwind('$current_career');

        aggregate.match({ $or: [
            { 'registration_to_career.studentRegistrationStatus': 'enrolled-standard' },
            { 'registration_to_career.studentRegistrationStatus': 'enrolled-for-exams' },
            { 'registration_to_career.studentRegistrationStatus': 'enrolled-with-a-scholarship' },
            { 'registration_to_career.studentRegistrationStatus': 'final-approved' },
            { 'registration_to_career.studentRegistrationStatus': 'conditional-approved' }
        ]});

        aggregate.match({'registration_to_career.academicYear': {'$eq': String(currentDate.getFullYear())}});

        aggregate.group({
            '_id' : "$_id",
            "personalData":{ "$first": "$personalData" },
            "user":{ "$first": "$user" },
            "registration_to_career":{ "$first": "$registration_to_career" },
            "current_career":{ "$first": "$current_career" }
        });

        aggregate.sort({'personalData.lastName': 1, 'personalData.firstName': 1});

        aggregate.exec(function (error, studentsFile) {
            if (error) return res.json({
                success: false,
                data: error,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                data: studentsFile,
                success: true
            });
        });
    }

};