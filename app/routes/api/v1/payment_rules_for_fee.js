var mongoose = require('mongoose');

module.exports = {

    list : function (req, res) {
        var aggregate = db.PaymentRulesForFee.aggregate();

        aggregate.lookup({
            "from": "careers",
            "localField": "career",
            "foreignField": "_id",
            "as": "career"
        });
        aggregate.unwind('$career');

        aggregate
            .exec(function (error, paymentRules) {
                if (error) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    data: paymentRules,
                    success: true
                });
            });
    },

    create : function (req, res) {
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;
        var interests = req.body.interests || req.query.interests || req.params.interests;
        var career = req.body.career || req.query.career || req.params.career;
        var baseAmount = req.body.baseAmount || req.query.baseAmount || req.params.baseAmount;
        var discountUntilDay = req.body.discountUntilDay || req.query.discountUntilDay || req.params.discountUntilDay || 0;
        var discountAmount = req.body.discountAmount || req.query.discountAmount || req.params.discountAmount || 0;
        var expirationDay = req.body.expirationDay || req.query.expirationDay || req.params.expirationDay;

        var rule = new db.PaymentRulesForFee({
            _id: new mongoose.Types.ObjectId(),
            name: name,
            active: active,
            interests: interests,
            career: career,
            baseAmount: baseAmount,
            discount: {
                untilDay: discountUntilDay,
                amount: discountAmount
            },
            expirationDay: expirationDay
        });

        rule.save(function (err, item_saved) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });
            return res.json({
                success: true,
                data: item_saved,
                message: 'La regla se ha creado exitosamente.'
            });
        });
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var name = req.body.name || req.query.name || req.params.name;
        var active = req.body.active || req.query.active || req.params.active || false;
        var interests = req.body.interests || req.query.interests || req.params.interests;
        var career = req.body.career || req.query.career || req.params.career;
        var baseAmount = req.body.baseAmount || req.query.baseAmount || req.params.baseAmount;
        var discountUntilDay = req.body.discount.untilDay || req.query.discountUntilDay || req.params.discountUntilDay || 0;
        var discountAmount = req.body.discount.amount || req.query.discountAmount || req.params.discountAmount || 0;
        var expirationDay = req.body.expirationDay || req.query.expirationDay || req.params.expirationDay;

        db.PaymentRulesForFee.findById(id, function (err, rule) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            rule.name = name;
            rule.active = active;
            rule.lastModified = Date.now();
            rule.interests = interests;
            rule.career = career;
            rule.baseAmount = baseAmount;
            rule.discount.untilDay = discountUntilDay;
            rule.discount.amount = discountAmount;
            rule.expirationDay = expirationDay;

            rule.save(function (err, item_saved) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });
                return res.json({
                    success: true,
                    data: item_saved,
                    message: 'La regla se ha guardado exitosamente.'
                });
            });
        })
    },

    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.PaymentRulesForFee.remove({'_id': id}, function (error, cmd) {
            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if (cmd.result.ok && cmd.result.n > 0) {
                return res.json({
                    success: true,
                    data: {
                        _id: id
                    },
                    message: 'La operación se ha realizado con éxito.'
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No se ha encontrado el registro que desea eliminar.'
                });
            }
        });
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.PaymentRulesForFee.findById(id, function (err, rule) {
            if (err) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            return res.json({
                success: true,
                data: rule,
                message: 'La operación se ha realizado con éxito.'
            });
        });
    }
};