var mongoose = require('mongoose');
var create_tuition_payment = require('../../../middlewares/create_tuition_payment');
var xlsx = require('excel4node');

module.exports = {

    list: function (req, res) {

        var dni = req.body.dni || req.query.dni || req.params.dni;
        var page = req.body.page || req.query.page || req.params.page || 1;
        var limit = req.body.limit || req.query.limit || req.params.limit || 10;

        var career = req.body.career || req.query.career || req.params.career;
        var level = req.body.level || req.query.level || req.params.level;
        var state = req.body.stateOfRegistrationToCareer || req.query.stateOfRegistrationToCareer || req.params.stateOfRegistrationToCareer;

        var academicYear = req.body.academicYear || req.query.academicYear || req.params.academicYear;

        var query = {};
        var options = {
            sort: { createdAt: -1 },
            populate: ['user', 'career', 'studentFile'],
            lean: true,
            page: Number(page),
            limit: Number(limit)
        };

        if(career) query.career = career;
        if(level) query.level = level;
        if(academicYear) {
            query = Object.assign(query, {'academicYear': academicYear});
        }

        if(state) {

            if (state == 'pending') {
                query.$or = [
                    { 'studentRegistrationStatus': 'pending' },
                    { 'studentRegistrationStatus': 'pending-step-1' },
                    { 'studentRegistrationStatus': 'pending-step-2' },
                    { 'studentRegistrationStatus': 'pending-step-3' }
                ];
            } else if (state == 'approved') {
                query.$or = [
                    { 'studentRegistrationStatus': 'conditional-approved' },
                    { 'studentRegistrationStatus': 'final-approved' }
                ];
            } else if (state == 'enrolled') {
                query.$or = [
                    { 'studentRegistrationStatus': 'enrolled-standard' },
                    { 'studentRegistrationStatus': 'enrolled-for-exams' },
                    { 'studentRegistrationStatus': 'enrolled-with-a-scholarship' }
                ];
            } else if (state == 'suspended') {
                query.$or = [
                    { 'studentRegistrationStatus': 'officially-suspended' },
                    { 'studentRegistrationStatus': 'suspended-ex-officio' }
                ];
            } else {
                query = Object.assign(query, {'studentRegistrationStatus': state});
            }

        }

        if (dni) {

            db.StudentFile.findOne({ 'personalData.dni': dni }, function (err, studentFile) {
                if (err) return res.json({
                    success: false,
                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                });

                if (studentFile) {

                    query = Object.assign(query, {'user': studentFile.user});

                    getRegistrationToCareer(query, options);

                } else {

                    return res.json({
                        success: true,
                        data: {
                            docs: [],
                            total: 0,
                            pages: 0,
                            page: 0
                        },
                        message: 'No se han encontrado resultados.'
                    });

                }

            });

        } else {

            getRegistrationToCareer(query, options);

        }

        function getRegistrationToCareer(query, options) {

            db.RegistrationToCareer.paginate(query, options, function(err, items) {
                if (err) {
                    //console.log(err);
                    return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde. '
                    });
                }

                return res.json({
                    success: true,
                    data: items,
                    message: 'La operación se ha realizado con éxito.'
                });
            });

        }
    },

    edit : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var user = req.body.user || req.query.user || req.params.user;
        var career = req.body.career || req.query.career || req.params.career;
        var level = req.body.level || req.query.level || req.params.level;
        var academicYear = req.body.academicYear || req.query.academicYear || req.params.academicYear;
        var studentRegistrationStatus = req.body.studentRegistrationStatus || req.query.studentRegistrationStatus || req.params.studentRegistrationStatus;
        var observations = req.body.observations || req.query.observations || req.params.observations;
        var payment = req.body.payment || req.query.payment || req.params.payment;
        var studentFile = req.body.studentFile || req.query.studentFile || req.params.studentFile;
        var repeatingTheClass = req.body.repeatingTheClass || req.query.repeatingTheClass || req.params.repeatingTheClass;
        var studyPlan = req.body.studyPlan || req.query.studyPlan || req.params.studyPlan;


        var params = {
            id: id,
            user: user,
            career: career,
            level: level,
            academicYear: academicYear,
            studentRegistrationStatus: studentRegistrationStatus,
            observations: observations,
            payment: payment,
            studentFile: studentFile,
            repeatingTheClass: repeatingTheClass,
            studyPlan: studyPlan
        };

        editRegistrationToCareer(params);

        function editRegistrationToCareer( params ) {

            if(params.id) {

                db.RegistrationToCareer.findById(params.id, function (err, item) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if (item) {

                        if (params.user) item.user = params.user;
                        if (params.career) item.career = params.career;
                        if (params.level) {
                            item.level = params.level;

                            if (params.repeatingTheClass) {
                                item.repeatingTheClass = params.repeatingTheClass;
                            } else {
                                item.repeatingTheClass = false;
                            }
                        }
                        if (params.academicYear) item.academicYear = params.academicYear;

                        if (params.studentRegistrationStatus) item.studentRegistrationStatus = params.studentRegistrationStatus;
                        if( item.studentRegistrationStatus == 'conditional-approved' || item.studentRegistrationStatus == 'final-approved'  ) {
                            create_tuition_payment(item);
                        }

                        if (params.observations) item.observations = params.observations;

                        if (params.studyPlan) item.studyPlan = mongoose.Types.ObjectId(params.studyPlan);

                        item.save(function (err, item_saved) {
                            if (err) return res.json({
                                success: false,
                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                            });

                            db.RegistrationToCareer.populate(item_saved, 'studentFile', function (err, item_saved) {

                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                return res.json({
                                    success: true,
                                    data: item_saved,
                                    message: 'Los datos se han guardado exitosamente.'
                                });

                            });

                        });

                    } else {

                        return res.json({
                            success: false,
                            message: 'No se ha encontrado el registro.'
                        });

                    }
                });

            } else {

                db.RegistrationToCareer.findOne({user: user, academicYear: academicYear}, function (err, item) {
                    if (err) return res.json({
                        success: false,
                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                    });

                    if (item) {

                        if (params.user) item.user = params.user;
                        if (params.career) item.career = params.career;
                        if (params.level) {
                            item.level = params.level;

                            if (params.repeatingTheClass) {
                                item.repeatingTheClass = params.repeatingTheClass;
                            } else {
                                item.repeatingTheClass = false;
                            }
                        }
                        if (params.academicYear) item.academicYear = params.academicYear;

                        if (params.studentRegistrationStatus) item.studentRegistrationStatus = params.studentRegistrationStatus;

                        if (params.observations) item.observations = params.observations;

                        if (params.payment && params.payment.receiptNumber) {
                            item.payment.receiptNumber = params.payment.receiptNumber
                        }

                        if(params.studentFile) item.studentFile = params.studentFile;

                        if (params.studyPlan) item.studyPlan = mongoose.Types.ObjectId(params.studyPlan);

                        item.save(function (err, item_saved) {
                            if (err) return res.json({
                                success: false,
                                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                            });

                            db.RegistrationToCareer.populate(item_saved, 'studentFile', function (err, item_saved) {

                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                return res.json({
                                    success: true,
                                    data: item_saved,
                                    message: 'Los datos se han guardado exitosamente.'
                                });

                            });
                        });

                    } else {

                        db.Career.findById(career, function (err, careerItem) {

                            var defaultStudyPlan = careerItem.studyPlans.filter(function (studyPlan) {
                                return studyPlan.isDefault;
                            });

                            item = new db.RegistrationToCareer({
                                _id: new mongoose.Types.ObjectId(),
                                user: user,
                                career: career,
                                level: level,
                                academicYear: academicYear,
                                studentRegistrationStatus: studentRegistrationStatus
                            });

                            if(defaultStudyPlan && defaultStudyPlan.length > 0) {
                                item.studyPlan = defaultStudyPlan[0]._id;
                            }

                            item.save(function (err, item_saved) {
                                if (err) return res.json({
                                    success: false,
                                    message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                });

                                db.RegistrationToCareer.populate(item_saved, 'studentFile', function (err, item_saved) {

                                    if (err) return res.json({
                                        success: false,
                                        message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                                    });

                                    return res.json({
                                        success: true,
                                        data: item_saved,
                                        message: 'Los datos se han guardado exitosamente.'
                                    });

                                });
                            });
                        });

                    }
                });

            }

        }
    },

    get : function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;
        var user = req.body.user || req.query.user || req.params.user;
        var academicYear = req.body.academicYear || req.query.academicYear || req.params.academicYear;

        var params = {
            id: id,
            user: user,
            academicYear: academicYear
        };

        getRegistrationToCareer( params );

        function getRegistrationToCareer( params ) {

            if (params.id) {

                db.RegistrationToCareer
                    .findById(id)
                    .populate('user')
                    .populate('career')
                    .populate('studentFile')
                    .exec(function (err, item) {
                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        return res.json({
                            success: true,
                            data: item,
                            message: 'La operación se ha realizado con éxito.'
                        });
                    });

            } else {

                db.RegistrationToCareer
                    .findOne({ user: user, academicYear: academicYear })
                    .populate('user')
                    .populate('career')
                    .populate('studentFile')
                    .exec(function (err, item) {

                        if (err) return res.json({
                            success: false,
                            message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
                        });

                        return res.json({
                            success: true,
                            data: item,
                            message: 'La operación se ha realizado con éxito.'
                        });
                    });

            }
        }
    },


    delete: function (req, res) {
        var id = req.body._id || req.query._id || req.params._id;

        db.RegistrationToCareer.remove({'_id': id}, function (error, cmd) {
            if (error) return res.json({
                success: false,
                message: 'Ha ocurrido un error. Intentelo nuevamente mas tarde.'
            });

            if (cmd.result.ok && cmd.result.n > 0) {
                return res.json({
                    success: true,
                    data: {
                        _id: id
                    },
                    message: 'La operación se ha realizado con éxito.'
                });
            } else {
                return res.json({
                    success: false,
                    message: 'No se ha encontrado el registro que desea eliminar.'
                });
            }
        });
    },




    download: function (req, res) {
        var dni = req.body.dni || req.query.dni || req.params.dni;
        var career = req.body.career || req.query.career || req.params.career;
        var level = req.body.level || req.query.level || req.params.level;
        var state = req.body.stateOfRegistrationToCareer || req.query.stateOfRegistrationToCareer || req.params.stateOfRegistrationToCareer;
        var academicYear = req.body.academicYear || req.query.academicYear || req.params.academicYear;

        var aggregate = db.RegistrationToCareer.aggregate();

        aggregate.lookup({
            "from": "users",
            "localField": "user",
            "foreignField": "_id",
            "as": "user"
        });
        aggregate.unwind('$user');

        aggregate.lookup({
            "from": "student_files",
            "localField": "user._id",
            "foreignField": "user",
            "as": "file"
        });
        aggregate.unwind('$file');

        aggregate.lookup({
            "from": "careers",
            "localField": "career",
            "foreignField": "_id",
            "as": "career"
        });
        aggregate.unwind('$career');

        if(career) {
            aggregate.match({'career._id': {'$eq': mongoose.Types.ObjectId(career)}});
        }

        if(level) {
            aggregate.match({'level': {'$eq': level}});
        }

        if (dni) {
            aggregate.match({'file.personalData.dni': {'$eq' : Number(dni)}})
        }

        if(academicYear) {
            aggregate.match({'academicYear': {'$eq': String(academicYear)}});
        }

        if(state) {

            if (state == 'pending') {
                aggregate.match({'$or': [
                    { 'studentRegistrationStatus': 'pending' },
                    { 'studentRegistrationStatus': 'pending-step-1' },
                    { 'studentRegistrationStatus': 'pending-step-2' },
                    { 'studentRegistrationStatus': 'pending-step-3' }
                ]});
            } else if (state == 'approved') {
                aggregate.match({'$or': [
                    { 'studentRegistrationStatus': 'conditional-approved' },
                    { 'studentRegistrationStatus': 'final-approved' }
                ]});
            } else if (state == 'enrolled') {
                aggregate.match({'$or': [
                    { 'studentRegistrationStatus': 'enrolled-standard' },
                    { 'studentRegistrationStatus': 'enrolled-for-exams' },
                    { 'studentRegistrationStatus': 'enrolled-with-a-scholarship' }
                ]});
            } else {
                aggregate.match({'studentRegistrationStatus': {'$eq': state}});
            }

        }

        //aggregate.group({"_id": "$_id"});

        aggregate.sort({ 'academicYear': -1 ,'career.name': 1, 'level': 1, 'file.personalData.lastName': 1, 'file.personalData.firstName': 1  });

        aggregate.exec(function (error, resultData) {
            if (error) return res.status(404).send("Ha ocurrido un error.");

            // Create a new instance of a Workbook class
            var wb = new xlsx.Workbook();

            // Add Worksheets to the workbook
            var ws = wb.addWorksheet('Inscriptos a las carreras', { disableRowSpansOptimization: false });

            // Create a reusable style
            var headStyle = wb.createStyle({
                font: {
                    color: '#ffffff',
                    size: 12,
                    bold: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '2172d7',
                }
            });

            ws.cell(1,1).style(headStyle);
            ws.cell(1,2).style(headStyle);
            ws.cell(1,3).style(headStyle);
            ws.cell(1,4).style(headStyle);
            ws.cell(1,5).style(headStyle);
            ws.cell(1,6).style(headStyle);
            ws.cell(1,7).style(headStyle);
            ws.cell(1,8).style(headStyle);
            ws.cell(1,9).style(headStyle);
            ws.cell(1,10).style(headStyle);
            ws.cell(1,11).style(headStyle);
            ws.cell(1,12).style(headStyle);
            ws.cell(1,13).style(headStyle);
            ws.cell(1,14).style(headStyle);
            ws.cell(2, 1)
                .string(' INSCRIPTOS A LAS CARRERAS')
                .style(headStyle);
            ws.cell(2,2).style(headStyle);
            ws.cell(2,3).style(headStyle);
            ws.cell(2,4).style(headStyle);
            ws.cell(2,5).style(headStyle);
            ws.cell(2,6).style(headStyle);
            ws.cell(2,7).style(headStyle);
            ws.cell(2,8).style(headStyle);
            ws.cell(2,9).style(headStyle);
            ws.cell(2,10).style(headStyle);
            ws.cell(2,11).style(headStyle);
            ws.cell(2,12).style(headStyle);
            ws.cell(2,13).style(headStyle);
            ws.cell(2,14).style(headStyle);
            ws.cell(3,1).style(headStyle);
            ws.cell(3,2).style(headStyle);
            ws.cell(3,3).style(headStyle);
            ws.cell(3,4).style(headStyle);
            ws.cell(3,5).style(headStyle);
            ws.cell(3,6).style(headStyle);
            ws.cell(3,7).style(headStyle);
            ws.cell(3,8).style(headStyle);
            ws.cell(3,9).style(headStyle);
            ws.cell(3,10).style(headStyle);
            ws.cell(3,11).style(headStyle);
            ws.cell(3,12).style(headStyle);
            ws.cell(3,13).style(headStyle);
            ws.cell(3,14).style(headStyle);

            ws.cell(5, 1)
                .string('#')
                .style(headStyle);

            ws.cell(5, 2)
                .string('DNI')
                .style(headStyle);

            ws.cell(5, 3)
                .string('APELLIDO')
                .style(headStyle);

            ws.cell(5, 4)
                .string('NOMBRE')
                .style(headStyle);

            ws.cell(5, 5)
                .string('FECHA DE NACIMIENTO')
                .style(headStyle);

            ws.cell(5, 6)
                .string('DOMICILIO')
                .style(headStyle);

            ws.cell(5, 7)
                .string('TALLA')
                .style(headStyle);

            ws.cell(5, 8)
                .string('SEXO')
                .style(headStyle);

            ws.cell(5, 9)
                .string('CARRERA')
                .style(headStyle);

            ws.cell(5, 10)
                .string('NIVEL')
                .style(headStyle);

            ws.cell(5, 11)
                .string('CICLO LECTIVO')
                .style(headStyle);

            ws.cell(5, 12)
                .string('ESTADO DE LA INSCRIPCIÓN')
                .style(headStyle);

            ws.cell(5, 13)
                .string('EMAIL')
                .style(headStyle);

            ws.cell(5, 14)
                .string('Nº DE CELULAR')
                .style(headStyle);

            var row = 6;
            for (var i=0, total=resultData.length; i<total; i++) {

                ws.cell(row, 1)
                    .number((i+1));

                ws.cell(row, 2)
                    .number( (resultData[i].file.personalData.dni ? resultData[i].file.personalData.dni : '') );

                ws.cell(row, 3)
                    .string( (resultData[i].file.personalData.lastName ? resultData[i].file.personalData.lastName : '') );

                ws.cell(row, 4)
                    .string( (resultData[i].file.personalData.firstName ? resultData[i].file.personalData.firstName : '') );

                ws.cell(row, 5)
                    .string( (resultData[i].file.personalData.birthDate ? resultData[i].file.personalData.birthDate.getDate() + '/' + (resultData[i].file.personalData.birthDate.getMonth() + 1) + '/' + resultData[i].file.personalData.birthDate.getFullYear() : '') );

                ws.cell(row, 6)
                    .string( ( resultData[i].file.personalData.currentResidence.street ? resultData[i].file.personalData.currentResidence.street + ' ' + resultData[i].file.personalData.currentResidence.streetNumber + ' - ' + resultData[i].file.personalData.currentResidence.neighborhood + ' - ' + resultData[i].file.personalData.currentResidence.locality + ' - ' + resultData[i].file.personalData.currentResidence.province + ' - ' + resultData[i].file.personalData.currentResidence.country : '') );

                ws.cell(row, 7)
                    .string( (resultData[i].file.personalData.size ? resultData[i].file.personalData.size : '') );

                var gender = '';
                if(resultData[i].file.personalData.gender && resultData[i].file.personalData.gender === 'male') {
                    gender = 'MASCULINO';
                } else if (resultData[i].file.personalData.gender && resultData[i].file.personalData.gender === 'female') {
                    gender = 'FEMENINO';
                }
                ws.cell(row, 8)
                    .string( gender );

                ws.cell(row, 9)
                    .string( (resultData[i].career.name ? resultData[i].career.name : '') );

                ws.cell(row, 10)
                    .string( (resultData[i].level ? resultData[i].level : '' ) );

                ws.cell(row, 11)
                    .string( (resultData[i].academicYear ? resultData[i].academicYear : '') );

                var studentRegistrationStatus = '';

                if (resultData[i].studentRegistrationStatus === 'enrolled-standard') {
                    studentRegistrationStatus = 'Matrículado'
                } else if (resultData[i].studentRegistrationStatus === 'enrolled-for-exams') {
                    studentRegistrationStatus = 'Matrículado para Exámenes'
                } else if (resultData[i].studentRegistrationStatus === 'enrolled-with-a-scholarship') {
                    studentRegistrationStatus = 'Matriculado con Beca'
                } else if (resultData[i].studentRegistrationStatus === 'final-approved') {
                    studentRegistrationStatus = 'Aprobado Definitivo'
                } else if (resultData[i].studentRegistrationStatus === 'conditional-approved') {
                    studentRegistrationStatus = 'Aprobado Condicional'
                } else if (resultData[i].studentRegistrationStatus === 'pending-step-1') {
                    studentRegistrationStatus = 'Pendiente'
                } else if (resultData[i].studentRegistrationStatus === 'pending-step-2') {
                    studentRegistrationStatus = 'Pendiente'
                } else if (resultData[i].studentRegistrationStatus === 'pending-step-3') {
                    studentRegistrationStatus = 'Pendiente'
                } else if (resultData[i].studentRegistrationStatus === 'pending') {
                    studentRegistrationStatus = 'Pendiente'
                } else if (resultData[i].studentRegistrationStatus === 'officially-suspended') {
                    studentRegistrationStatus = 'Baja Definitiva'
                } else if (resultData[i].studentRegistrationStatus === 'suspended-ex-officio') {
                    studentRegistrationStatus = 'Baja de Oficio'
                }

                ws.cell(row, 12)
                    .string( studentRegistrationStatus );

                ws.cell(row, 13)
                    .string( (resultData[i].user.email ? resultData[i].user.email : '') );

                ws.cell(row, 14)
                    .number( (resultData[i].file.personalData.currentResidence.cellPhone ? resultData[i].file.personalData.currentResidence.cellPhone : '') );

                row++;
            }

            if(resultData.length === 0) {
                ws.cell(row, 1)
                    .string('No se han encontrado inscripciones.');
            }

            wb.write('inscriptos_a_carreras.xlsx', res);
        });
    },



    sincronizeRegistrationToCareers: function (req, res) {
        db.RegistrationToCareer.find({}).then( function( items) {

            items.forEach( function (item) {
                if (item.progress.step1Completed) {
                    item.studentRegistrationStatus = 'pending-step-1';
                }
                if (item.progress.step2Completed) {
                    item.studentRegistrationStatus = 'pending-step-2';
                }
                if (item.progress.step3Completed) {
                    item.studentRegistrationStatus = 'pending-step-3';
                }
                if (item.progress.approved.finalApproved) {
                    item.studentRegistrationStatus = 'final-approved';
                }
                if (item.progress.approved.conditionalApproved) {
                    item.studentRegistrationStatus = 'conditional-approved';
                }
                if (item.progress.enrolled) {
                    item.studentRegistrationStatus = 'enrolled-standard';
                }

                item.save();
            });
        });

        return res.json({
            success: false,
            message: 'Sincronizando.'
        });
    }
};