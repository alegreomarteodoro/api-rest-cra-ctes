process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);

describe('Token', () => {

    describe('/POST /api/v1/authenticate', () => {
        it('it should POST authentication success', (done) => {
            var user = {
                email: 'omar@test.com',
                password: "test"
            };

            chai.request(server)
                .post('/api/v1/authenticate')
                .send(user)
                .end((err, res) => {
                    //console.log(res.body.token);
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

    describe('/POST /api/v1/authenticate', () => {
        it('it should POST authentication error', (done) => {
            var user = {
                email: 'omar@test.com',
                password: "la_contraseña_esta_mal"
            };

            chai.request(server)
                .post('/api/v1/authenticate')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Email o contraseña inválidos.');
                    done();
                });
        });
    });

});