process.env.NODE_ENV = 'test';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server');
var should = chai.should();

chai.use(chaiHttp);

describe('Users', () => {
    before((done) => {
        db.User.remove({}, (err) => {
            done();
        });
    });

    describe('/POST /api/v1/users', () => {
        it('it should POST a user', (done) => {
            var user = {
                email: 'omar@test.com',
                password: "test"
            };

            chai.request(server)
                .post('/api/v1/users')
                .send(user)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
            });
        });
    });

    describe('/GET /api/v1/users', () => {
        it('it should GET all the users', (done) => {
            chai.request(server)
                .get('/api/v1/users')
                .set('x-access-token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7Il9pZCI6IjU5ZDE3MjVjMmNmOWJlMDM4Y2QzODA5ZiIsInNhbHQiOiI4NzJhZDcwMmUzNjIyNTI2MjVjNWFhOTAwZjFkOTc5NDU5OTVjMTQ0NDhhMzM3ZjQ5MDQwZjA2ODYwYjdmOTU5IiwiaGFzaCI6IjI2MTZmNGE3ODdlMTY5YzVhZGUxNDQ4Y2UwODk2MDRiY2QzY2RjODZhMmY1MmIyZjIyNzEwMWYyYjZkYWZkNTQ4MDIyMjdjNmM0NjliNTkyZGIyMWM1ZjdmNmQ3OTc0ZTI2N2Y3NzRmOTI4YWJhZjk3YmQ1NjE3OGY1ZTE4MDcxNDI1MjgxMzRhMjNlNGQ4ZjAxZmViZmQ5YjJhOTRmYjBhNzJhZGJjY2ExNjYyYTE2ZDI4MjU0ODY5NWQwMWU3ZjczMDZkZWMyZGQ5NzBjOTNjZGY3YTc0YWQxNmU0NTRmOTNjNGJkZTk4MzI3YjNlY2QzMmVmNTIwZGY5NzIzMzk1YmNiYjRhMDE5Y2E0NDg4YjlhYmZlNWFhNTg5ZDMwMDZkN2RkMGEwZGQ2M2YwMzlkYmRkMjAxMWU1M2I3YjNmMjU4ZDAzODBjNDc4NWE2MzY4ODY1MTExZTNhYzQ0N2I0ZjZjN2UzOWZmYTdkMDZkNGU4NDMyN2I4YjMxMjZiOWZiNzNiMGEzMTMzZTVlZTZhOTVmYTU5N2E4ZGFiYmQzYzg5YjYyNzc2MWQwY2MzOWI4ZDM3ZmIwOTUxM2E4MDU1MTM2YzBjYzg4MWRhY2VmYTFiNmZiYWZhOTEyMjQ4N2JhYTU0NjVkMDFiNWQwNGRmOGJlMDA0MWQ5NmJlMzJhNmY5M2IwNDNlY2RkNmFlNzVhNzc5MjkwMGZkNWI0Nzg3YmNlOWYxZjlkYThhMmUzOWU0OTAzNTg1Mzg0MDA1ODY0NmY1MmNmN2Y5ZDE3YWVkNzg0YmJkODBlODFkMGJhNGVjYThlMmFjNTU4NTRjOGFkMjY4MmZhNTQyNGY2ZjIyM2M5MjNmODM3NDFhNzYxNWMxOWQ0ZTRiM2RiYTQ4OGFhOTQ3NGE1YzYwNmI2MWM5ODM0ZTZhNjFhYThmNGI1MDdjODJlZmI4MDkxNzU1YmQzMjkxYjIxMzkxMTcxYjVmMmNmOWNiMzQxYzYzZGJlZDBlYTIzNDNmNjU1NzIxNTExMDIxNzA0NzIxYmFhNzc1NjkxMGY2YWE1YTY3M2Y2Zjc4NjdjNzMyNzU2OGJjNDhlNDYyODA4NjQ5NGE3YmQzN2ZkZWY4MTA5OTY3NGJiZTQ2ZGI4MmEzYjY3YjMyMjkxOGJkZDQ5ZDcxNGFmOTQ0ZTNjZmIxYjNkOWNlNjA1ODA3OWQ5MTQ0MTYxY2ZkMTU3ODM1YzFhM2Q2MWY0MzIyOGFjOTY4MTc5NTQ3Njc1MDg1ZmY4MmIzYTI2OGJmNGNhMzQ2OTFiYTZmMWI0Mjk0NmQ2NGJhZDFiMmU4NWFkMjkxNzAwYWYwNWFmYWQ0ZmFkMjI2YWRkZGIxYzIzNWQiLCJlbWFpbCI6Im9tYXJAdGVzdC5jb20iLCJfX3YiOjAsImFkbWluIjpmYWxzZSwiYWN0aXZlIjp0cnVlLCJsYXN0TW9kaWZpZWQiOiIyMDE3LTEwLTAxVDIyOjU1OjI0Ljk4OVoiLCJjcmVhdGVkQXQiOiIyMDE3LTEwLTAxVDIyOjU1OjI0Ljk4OVoifSwiaWF0IjoxNTA2ODk4NjE4LCJleHAiOjE1MDc1MDM0MTh9._4de7AMWhPtfVbKQjh1xSb8SbHYcDM1HPOZ7KIradBs')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });

});