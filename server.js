var express = require('express');
var http = require('http');
var app = module.exports = express();
var autoloader = require('./core/libs/autoloader');

autoloader.environments('./config/environments', app);
autoloader.initializers('./config/initializers', app);
autoloader.routes('./app/routes', function(routes){
    require('./config/routes')(app, express, routes);
});

process.env.TZ = 'America/Argentina/Buenos_Aires';

var ipaddress = process.env.IP || app.get('ip') || "0.0.0.0";
var port = process.env.PORT || app.get('port') || 3000;
http.createServer(app).listen(port, ipaddress, function(){
    console.log('CRA server listening on port ' + port);
});