var fs = require('fs');
var path = require('path');

module.exports = {
    resolve : function(filename, ignore, app){
        var self = this;
        var stats = fs.lstatSync(filename),
            route = {};

        if (stats.isDirectory()) {
            return fs.readdirSync(filename).map(function(child) {
                return self.resolve(filename + '/' + child, ignore, app);
            });
        } else {
            route['filename'] = path.basename(filename);
            route['path'] = filename;
            route['status'] = 'loaded';
            if(ignore instanceof Array && ignore.length > 0){
                if(filename.match(ignore.join('|')) != null){
                    route['status'] = 'ignored';
                    return route;
                }
            }
            require(path.resolve(filename.replace('.js', '')))(app);
        }

        return route;
    }
}