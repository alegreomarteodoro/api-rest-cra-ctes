var path = require('path');
var route = require('./route');
var tree_directory = require('./tree_directory');

module.exports = {

    routes : function (dir, callback)
    {
        return callback(route.resolve(dir).routes);
    },

    initializers : function (dir, app)
    {
        var inits = tree_directory.resolve(dir, [], app);
        return inits;
    },

    environments : function (dir, app)
    {
        var env = tree_directory.resolve(dir, [], app);
        return env;
    }

};